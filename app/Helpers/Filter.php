<?php

namespace App\Helpers;

use App\Statement;

class Filter
{
    public function gtb($statement, $customer_id, $report_id)
    {

        $statement = $this->purge($statement);
       
        $new = explode(',', $statement[1]);
        unset($statement[1]);
        array_pop($statement);
        array_shift($statement);
        sort($statement);
        
        $statement = str_replace('"', '', $statement);
        $statement = array_map(function ($x) {
            return preg_replace_callback('/(\"\d+\,\d{3}(\"|\,|\.))/s', function ($m) {
                return str_replace(',', '', $m[0]);
            }, $x);
        }, $statement);
       
      //  dd($statement);
        $final = [];
        foreach ($statement as $key => $line) {
            $element = explode(',', $line);
            $final[] = $element;

        }
//dd($final);
        // $final = [];
        // foreach ($statement as $key => $line) {
        //     for ($i = 0; $i < count($statement); $i++) {
        //         $element = explode(',', $statement[$i]);
        //         foreach ($new as $key => $val) {
        //             $newArr[$val] = $element[$key];
        //             $final[$i] = $newArr;
        //         }
        //     }

        // }
     //  dd($final);
        foreach ($final as $last) {
            $data = array(
                'customer_id' => $customer_id,
                'transaction_date' => $last[0],
                'debit' => $last[3],
                'credit' => $last[4],
                'balance' => $last[5],
                //  'reference' => $last['Reference'],
                'report_id' =>$report_id,
                'remarks' => $last[6],
            );
         $statement = Statement::create($data);

        }

       // dd($data);
        return $statement;

    }

    public function ecobank($statement, $customer_id,$report_id)
    {

        $statement = $this->purge($statement);
       
        $statement = array_slice($statement, 2);

        $statement = array_map(function ($x) {
            return preg_replace_callback('/(\"\d+\,\d{3}(\"|\,|\.))/s', function ($m) {
                return str_replace(',', '', $m[0]);
            }, $x);
        }, $statement);
        
        foreach($statement as $key =>$one){
        if (strpos($one, 'Date,Description,') !== false) {
            unset($statement[$key]);
        }
    }

     //   dd($statement);
        $test = str_replace('"', '', $statement);

        $final = [];
        foreach ($test as $key => $line) {
            $element = explode(',', $line);
            $final[] = $element;

        }
//dd($final);
        foreach ($final as $last) {
            $data = array(
                'customer_id' => $customer_id,
                'transaction_date' => $last[0],
                'debit' => $last[4],
                'credit' => $last[5],
                'report_id' =>$report_id,
                'balance' => $last[6],
                'remarks' => $last[1],
            );

             $statement = Statement::create($data);
        }
      //  dd($data);
        return $statement;

    }

    public function scb($statement, $customer_id, $report_id)
    {

        $statement = $this->purge($statement);
        $header = explode(',', $statement[3]);
        $statement = array_slice($statement, 4);

        //remove comma for money values
        $statement = array_map(function ($x) {
            return preg_replace_callback('/"\d{1,3}(?:,\d{3})+\.\d{2}"/', function ($m) {
                return str_replace(',', '', $m[0]);
            }, $x);
        }, $statement);

        $statement = array_slice($statement, 4);
        
        
        $statement = str_replace('"', '', $statement);
         
        $final = [];
        foreach ($statement as $key => $line) {
            $element = explode(',', $line);
            $final[] = $element;

        }
       
        foreach ($final as $last) {
            $data = array(
                'customer_id' => $customer_id,
                'transaction_date' => $last[0],
                'debit' => $last[3],
                'credit' => $last[2],
                 'balance' => $last[4],
                 'report_id' =>$report_id,
                'remarks' => $last[1],
            );

            $statement = Statement::create($data);
        }

    }
    public function uba($statement, $customer_id, $report_id)
    {

        $statement = $this->purge($statement);
        $header = explode(',', $statement[3]);
        $statement = array_slice($statement, 4);
        $statement = array_map(function ($x) {
            return preg_replace_callback('/"\d{1,3}(?:,\d{3})+\.\d{2}"/', function ($m) {
                return str_replace(',', '', $m[0]);
            }, $x);
        }, $statement);
       
        $test = str_replace('"', '', $statement);
       

        $final = [];
        foreach ($test as $key => $line) {
            for ($i = 0; $i < count($test); $i++) {
                $element = explode(',', $test[$i]);
                foreach ($header as $key => $val) {
                    $newArr[$val] = $element[$key];
                    $final[$i] = $newArr;
                }
            }

        }
       // dd($final);
        foreach ($final as $last) {
            $data = array(
                'customer_id' => $customer_id,
                'transaction_date' => $last['Transaction Date'],
                'debit' => $last['Withdrawal'],
                'credit' => $last['Deposit'],
                'balance' => $last['Account'],
                'report_id' =>$report_id,
                'remarks' => $last['Transaction Remarks'],
            );
            $statement = Statement::create($data);
        }
        return $statement;
    }
    public function zenith($statement)
    {

        $statement = $this->purge($statement);
        // dd($statement);
        $header = explode(',', $statement[13]);

        $statement = array_slice($statement, 3);

        foreach ($statement as $key => $one) {
            if (strpos($one, '/') == false) {
                unset($statement[$key]);
            }

        }
        //   dd($statement);

        $final = [];
        foreach ($statement as $line) {
            $true = substr_replace($line, "    ", 8, 0);

            $true = substr_replace($true, "    ", 20, 0);
            $final[] = substr_replace($true, "    ", 28, 0);

        }

        // dd($final);
        foreach ($final as $key => $one) {
            if (strpos($one, 'FRAUD AL') !== false || strpos($one, 'Please d') !== false ||
                strpos($one, 'This is') !== false || strpos($one, 'If we do') !== false ||
                strpos($one, 'Account     Statemen') !== false || strpos($one, 'you of t') !== false ||
                strpos($one, 'From') !== false) {
                unset($final[$key]);
            }

        }
        $test = str_replace(',"', ',    "', $final);
        $test = str_replace('",', '",    ', $test);
        $test = str_replace(',', '', $test);
        array_pop($test);
        array_pop($test);
        dd($test);
    }
    public function fcmb($statement, $customer_id, $report_id)
    {

        $statement = $this->purge($statement);
        //   dd($statement);
        $header = explode(',', $statement[7]);

        $statement = array_slice($statement, 8);
        //dd($statement);
        foreach ($statement as $key => $state) {

            if (strpos($state, 'Adhoc') !== false || strpos($state, 'Closing Balance') !== false
                || strpos($state, 'complaints kindly') !== false) {
                unset($statement[$key]);
            }
        }
        $statement = str_replace('Cr', '', $statement);

        $statement = array_map(function ($x) {
            return preg_replace_callback('/"\d{1,3}(?:,\d{3})+\.\d{2}"/', function ($m) {
                return str_replace(',', '', $m[0]);
            }, $x);
        }, $statement);

        $statement = str_replace('"', '', $statement);
        $final = [];
        foreach ($statement as $key => $line) {
            $element = explode(',', $line);
            $final[] = $element;

        }
       // dd($final);
        foreach ($final as $last) {
            $data = array(
                'customer_id' => $customer_id,
                'transaction_date' => $last[0],
                'debit' => $last[5],
                'credit' => $last[4],

                'balance' => $last[6],
                'remarks' => $last[2],
                'report_id' =>$report_id
            );

             $statement = Statement::create($data);
        }
       
//dd($statement);
    }

    public function purge($statement)
    {

        foreach ($statement as $key => $one) {

            if (strpos($one, 'Table') !== false) {
                unset($statement[$key]);
            }

        }
        return $statement;
    }
}
