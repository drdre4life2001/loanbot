<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use SoapClient;

class CheckBvn
{
  public function verify($bvn){
  
    $url = "https://api.paystack.co/bank/resolve_bvn/{$bvn}";

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    curl_setopt($ch, CURLOPT_HTTPHEADER,[
      'Authorization: Bearer sk_test_1072deb504fab84844adb749a1e0a6d34a7c5c2e'
     //  'Authorization: Bearer sk_live_414633379e2c96cf8206f221c2efa81c4dfa9d08'
    ]);


    $output = curl_exec($ch);
if($errno = curl_errno($ch)) {
    $error_message = curl_strerror($errno);
    $error =  "cURL error ({$errno}):\n {$error_message}";
dd($error);
}
   // dd($output);
    curl_close($ch);

    $arr = json_decode($output, true);

      return $arr;
    
  }

}
