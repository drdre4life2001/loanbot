<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class LoanCalculator
{
  public function calculate($salary, $existing_repyment, $customer_id){


   //  dd($existing_repyment);
     $loan_tenure = DB::table('customer_reports')->where('customer_id', $customer_id)->value('loan_tenure');
     $rate = 0.0495;
     $dti = 0;
     if($salary){

        if($salary>=50000 && $salary <=150000){    

            $dti = 0.333;
        }elseif($salary > 150000 && $salary <= 249000){
        $dti = 0.40;
  
        }elseif($salary>=250000){
            $dti = 0.50;
        } 
    //  dd($dti);

    $max_repayment = round( ($salary * $dti) - $existing_repyment, 2);

    $max_loan_amount = round(($max_repayment * $loan_tenure)/ ( 1+($rate * $loan_tenure)), -1);

    $repayment = $max_loan_amount * ( 1 + $rate * $loan_tenure)/$loan_tenure;
   
    $analysis_array = [
        'max_repayment' => $max_repayment,
        'max_loan_amount' =>$max_loan_amount,
        'repayment_amount' => $repayment,
    ];

     }
    return $analysis_array; 
  }

  public function check($salary, $existing_repyment, $loan_tenure){

    $rate = 0.0495;
     $dti = 0;
     if($salary){

      if($salary>=50000 && $salary <=150000){    

          $dti = 0.333;
      }elseif($salary > 150000 && $salary <= 249000){
      $dti = 0.40;

      }elseif($salary>=250000){
          $dti = 0.50;
      } 
         //dd($dti);

    $max_repayment =  ($salary * $dti) - $existing_repyment;

    $max_loan_amount = round($max_repayment * $loan_tenure / (1+($rate * $loan_tenure)), -1);

    $repayment = $max_loan_amount * ( 1 + $rate * $loan_tenure) / $loan_tenure;
   
    $analysis_array = [

        'max_repayment' => $max_repayment,
        'max_loan_amount' =>$max_loan_amount,
        'repayment_amount' => $repayment,
    ];

     }
    return $analysis_array; 
}
  
}
