<?php
namespace App\Helpers;

use App\Statement;
use DB;

class StatementAnalysis
{
    public function analyse($statement, $customer_id, $report_id)
    {

        //sull all credit transfer
        $sum_cr_transfer = Statement::where('remarks', 'LIKE', '%' . 'transfer' . '%')->where('credit', '>=', 1)
            ->where('customer_id', $customer_id)->where('report_id', $report_id)->sum('credit');

        $sum_debit_transfer = Statement::where('remarks', 'LIKE', '%' . 'atm' . '%')
            ->where('debit', '>=', 1)->where('customer_id', $customer_id)->where('report_id', $report_id)->sum('debit');

        $sum_charge = Statement::where('remarks', 'LIKE', '%' . 'charge' . '%')->where('debit', '>=', 1)->
            where('customer_id', $customer_id)->where('report_id', $report_id)->sum('debit');

        $pos_sum = Statement::where('remarks', 'LIKE', '%' . 'POS' . '%')->where('debit', '>=', 1)->
            where('customer_id', $customer_id)->where('report_id', $report_id)->sum('debit');

        //sum repayments

        $sum_repayment = Statement::where('remarks', 'LIKE', '%' . 'repayment' . '%')
            ->orwhere('remarks', 'LIKE', '%' . 'loan' . '%')->orwhere('remarks', 'LIKE', '%' . 'credit' . '%')->where('debit', '>=', 1)
            ->where('customer_id', $customer_id)->where('report_id', $report_id)->sum('debit');

        $salary_sum = Statement::where('remarks', 'LIKE', '%' . 'salary' . '%')->where('credit', '>=', 1)->
            where('customer_id', $customer_id)->where('report_id', $report_id)->sum('credit');

        $allsalaries = Statement::where('remarks', 'LIKE', '%' . 'salary' . '%')->where('credit', '>=', 1)->
            where('customer_id', $customer_id)->where('report_id', $report_id)->get();

            $allrepayments = Statement::where('remarks', 'LIKE', '%' . 'repayment' . '%')->where('debit', '>=', 1)->where('report_id', $report_id)->orwhere('remarks', 'LIKE', '%' . 'credit' . '%')
            ->where('debit', '>=', 1)->where('report_id', $report_id)->orwhere('remarks', 'LIKE', '%' . 'loan' . '%')
            ->where('debit', '>=', 1)->where('report_id', $report_id)->get();

        // dd($allsalaries);
        // $statements = Statement::where('customer_id', $customer_id)->where('report_id', $report_id)->get();
        //dd($statements);
        // update salary category
        $others = DB::table('statements')->where('category', null)->where('report_id', $report_id)->get();

        if(count($others) > 0 ){
        $charges = Statement::where('remarks', 'LIKE', '%' . 'charge' . '%')->orwhere('remarks', 'LIKE', '%' . 'stamp' . '%')->where('debit', '>=', 1)->
           orwhere('remarks', 'LIKE', '%' . 'commision' . '%')->orwhere('remarks', 'LIKE', '%' . 'tax' . '%')-> where('report_id', $report_id)->get();

        $pos = Statement::where('remarks', 'LIKE', '%' . 'POS' . '%')->where('debit', '>=', 1)->
            where('report_id', $report_id)->get();

        $atm = Statement::where('remarks', 'LIKE', '%' . 'atm' . '%')->where('debit', '>=', 1)->
            where('report_id', $report_id)->get();

        $transfers = Statement::where('remarks', 'LIKE', '%' . 'transfer' . '%')->orwhere('remarks', 'LIKE', '%' . 'trf' . '%')->where('debit', '>=', 1)->
            where('report_id', $report_id)->get();

        //$category = DB::table('statement_category')->get();
       // $keyword = DB::table('category_keyword')->get();
        //  foreach ($statements as $statement){
        //    foreach($keyword as $word){
        //        if(strpos($statement->remarks, $word->keyword) !== false){
        //         // dd($word->sc_name);
        //         $test = DB::table('statements')->where('id', $statement->id)->update(array(
        //           'category' => $word->keyword
        //       ));
        //      // dd($test);
        //        }
        //  }}
        //  dd($keyword);
        foreach ($allsalaries as $salary) {

            $test = DB::table('statements')->where('id', $salary->id)->where('report_id', $report_id)->update(array(
                'category' => 'Salary',
            ));
        }
        foreach ($charges as $charge) {

            $test = DB::table('statements')->where('id', $charge->id)->update(array(
                'category' => 'Charges',
            ));
        }

        foreach ($pos as $posweb) {

            $test = DB::table('statements')->where('id', $posweb->id)->update(array(
                'category' => 'POS/WEB',
            ));
        }
        foreach ($atm as $withrawal) {

            $test = DB::table('statements')->where('id', $withrawal->id)->update(array(
                'category' => 'ATM Withdrawal',
            ));
        }
        foreach ($transfers as $transfer) {

          $test = DB::table('statements')->where('id', $transfer->id)->update(array(
              'category' => 'Transfer',
          ));
      }
      foreach ($allrepayments as $repayment) {
                $update = DB::table('statements')->where('id', $repayment->id)->update(array(
                    'category' => 'Repayment',
                ));
            }

    }
        //dd($test);
        $analysis = [
            'credit_transfer' => $sum_cr_transfer,
            'debit_transfer' => $sum_debit_transfer,
            'pos' => $pos_sum,
            'charges' => $sum_charge,
            'repayment' => $sum_repayment,
            'salary' => $salary_sum,
        ];
        // dd($analysis);
        return $analysis;

    }

    public function summary($customer_id, $report_id)
    {
        $statements = Statement::where('customer_id', $customer_id)->where('report_id', $report_id)
            ->get();

        $total_credit = Statement::where('customer_id', $customer_id)->where('report_id', $report_id)->sum('credit');
        $total_debit = Statement::where('customer_id', $customer_id)->where('report_id', $report_id)->sum('debit');

        //minimum balace
        //$min_balance = Statement::where('customer_id',$customer_id)->where('debit' > 1)->max('debit');
        $balance = [];
        foreach ($statements as $statement) {
            $balance[] = $statement->balance;
        }
        $opening_balance = $statements[0]->balance;

        $balance = array_filter($balance);
        $max_balance = max($balance);
        $min_balance = min($balance);

        $sum = array_sum($balance);
        $count = count($balance);
        $avg_balance = round($sum / $count, 2);

        $summary = [
            'total_credit' => $total_credit,
            'total_debit' => $total_debit,
            'minimum_balance' => $min_balance,
            'maximum_balance' => $max_balance,
            'average_balance' => $avg_balance,
            'opening_balance' => $opening_balance,
        ];
        return $summary;

        // dd($max_balance);

    }

    public function dailybalance($customer_id, $report_id){

    
        $statements = DB::select("SELECT transaction_date, balance FROM statements where report_id = $report_id");
       //dd($statementss);
       
        $statement = [];
        foreach($statements as $balance)
        {
           $balance->transaction_date = date('m-d-Y', strtotime($balance->transaction_date));
           $statement[] = $balance;
        }

        $_data = [];
        foreach ($statement as $v) {
          if (isset($_data[$v->transaction_date])) {
            // found duplicate
            continue;
          }
          // remember unique item
          $_data[$v->transaction_date] = $v;
        }

        $data = array_values($_data);
       // $new = array_slice($data,35);
       $ndata = arsort($data);
       
       //dd($data);
        return $data;
       
    }

}
