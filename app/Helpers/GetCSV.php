<?php
namespace App\Helpers;

class GetCSV
{
    public function getcsv($customer_id, $file_name, $bank_short)
    {

        $num = rand(100, 9999);
        $co = bin2hex(random_bytes(5)) . '_' . $customer_id;
        $num = $num . '_' . $co;
        $ch = curl_init();
        //dd($file_name);
        // $file_name = urlencode($file_name)
        curl_setopt($ch, CURLOPT_URL, "http://104.154.164.127/?file=http://206.189.223.121/storage/$file_name.pdf&id=$num&bank=$bank_short");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $csv = curl_exec($ch);

        $err = curl_error($ch);

        curl_close($ch);

        return $csv;
    }

}
