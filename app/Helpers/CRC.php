<?php

namespace App\Helpers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Validator;
use SoapClient;

class CRC
{
    
    
    
    public function creditsummary($bvn, $customer_id)
    {  
        $client = new SoapClient("https://webserver.creditreferencenigeria.net/crcweb/liverequestinvoker.asmx?WSDL");
        
        $params = [
          'strUserID' => '9901914credit',
          'strPassword' => 'cR3ditD1r3ct2oi8',
          'strRequest' => '<REQUEST REQUEST_ID="1">,
          "<REQUEST_PARAMETERS>
            <REPORT_PARAMETERS RESPONSE_TYPE="1" SUBJECT_TYPE="1" REPORT_ID="104"/>
            <INQUIRY_REASON CODE="1"/>
            <APPLICATION CURRENCY="NGN" AMOUNT="0" NUMBER="0" PRODUCT="017"/>
          </REQUEST_PARAMETERS>
          <SEARCH_PARAMETERS SEARCH-TYPE="4">
          <BVN_NO>'.$bvn.'</BVN_NO>
          </SEARCH_PARAMETERS>
        </REQUEST>'
      ];  
try{

    $response = $client->PostRequest($params);
    $xml = simplexml_load_string($response->PostRequestResult);

}catch(\Exception $e){

}
     if(isset($response)){
      $xml = simplexml_load_string($response->PostRequestResult);
    
      //dd($xml);
     if(isset($xml->BODY->CREDIT_SCORE_DETAILS)){
       //  dd('score');
        $credit_score = DB::table('crc_credit_rating')->insertGetId(array( 
            'customer_id' =>$customer_id,
            'credit_score' => $xml->BODY->CREDIT_SCORE_DETAILS->CREDIT_SCORE_SUMMARY->CREDIT_SCORE,
            'credit_rating' =>$xml->BODY->CREDIT_SCORE_DETAILS->CREDIT_SCORE_SUMMARY->CREDIT_RATING,
        ));  
     }
      if(isset($xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE)){
       //dd('summary');
      $report = DB::table('crc_report')->insertGetId(array(
        'customer_id' => $customer_id,
        'provider_source' => $xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->PROVIDER_SOURCE,
        'facilities_count' =>$xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->FACILITIES_COUNT,
        'performing_facility' =>$xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->PERFORMING_FACILITY,
        'non_performing' => $xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->NONPERFORMING_FACILITY,
        'approved_amount' => $xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->APPROVED_AMOUNT,
        'account_balance' =>$xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->ACCOUNT_BALANCE,
        'overdue_amount' => $xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->OVERDUE_AMOUNT,
        'bureau_currency' => $xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->BUREAU_CURRENCY,
        'dishonored_cheques' =>$xml->BODY->MFCREDIT_SUMMARY->CURRENCY->SUMMARY_OF_PERFORMANCE->DISHONORED_CHEQUES_COUNT
      ));
    }
}else{
       
        return json_encode(array(
            "status" => "fail",
            "code" => "99"
        ));
    }

        if(isset($report)){
            return json_encode(array(
                "status" => "success",
                "code" => "00"
            ));
        }else {
            return json_encode(array(
                "status" => "fail",
                "code" => "99"
            ));
        }
   
     // dd($response);
    }
    
    
    
  
}
