<?php
namespace App\Helpers;

use DB;
use Illuminate\Support\Facades\Auth;
use SoapClient;

class MyBankStatement
{

    public function __construct($a)
    {
        $this->params = $a;
    }

    public function sendStatement()
    {

        // DB::table('statement')
        //     ->where([
        //         ['statement_user_id', $this->params['id']],
        //         ['statement_status', '<>', 1],
        //         ['statement_account_number', $this->params['account']],
        //     ])
        //     ->delete();

        // $activestatement = DB::table('statement')
        //     ->where([
        //         ['statement_user_id', $this->params['id']],
        //         ['statement_status', 1],
        //         ['statement_account_number', $this->params['account']],
        //     ])
        //     ->first();

        // if (empty($activestatement)) {
        // dd($this->params);
        $client = new SoapClient("https://mybankstatement.net/TPServices/webservice.asmx?wsdl", array('exceptions' => 0));

        $params = array(
            "AccountNo" => $this->params['account'],
            "BankID" => $this->params['bankid'],
            "ClientID" => '127',
            "StartDate" => $this->params['start'],
            "EndDate" => $this->params['end'],
            "Role" => $this->params['role'],
            "username" => '',
            "country" => $this->params['country'],
            "phone" => $this->params['phonenumber'],
            "ApplicantNames" => $this->params['name'],
            "ClientPasscode" => 'CLGif1*$pQPyMbfd0',
        );
        // dd($params);
        $response = $client->RequestStatement($params);
        if (is_soap_fault($response)) {
            return back()->withInput()->with('statementerror', 'connectivity mishap, please try again later');
        } else {

            $response = $response->RequestStatementResult;
            $response = (int) $response;

            if ($response < 0 || $response == 99) {
                return back()->withInput()->with('msg', 'Invalid credential, check and try again');
            } else {

                $id = DB::table('statement')
                    ->insertGetId(array(
                        'statement_user_id' => Auth::user()->id,
                        'statement_bank' => $this->params['bankid'],
                        'statement_account_number' => $this->params['account'],
                        'statement_account_applicants' => $this->params['id'],
                        'statement_requestid' => '',
                        'statement_account_tenor' => 3,
                        'statement_account_period' => $this->params['start'] . ' to ' . $this->params['end'],
                        'statement_account_category' => 'INDIVIDUALS',
                        'statement_account_bvn' => 'N/A',
                        'statement_account_currency' => 'NGN',
                        'statement_token' => 1,
                    ));

            }

            $msg = 'success';
        }
        // } else {
        //     $msg = 'account already verified';
        // }

        return $msg;

    }

    public function getstatement()
    {
        $client = new SoapClient("https://mybankstatement.net/TPServices/webservice.asmx?wsdl", array('exceptions' => 0));

        $params = array(
            "AccountNo" => $this->params['account'],
            "BankID" => $this->params['bankid'],
            "ClientID" => '127',
            "StartDate" => $this->params['start'],
            "country" => $this->params['country'],
            "EndDate" => $this->params['end'],
            "Role" => $this->params['role'],
            "username" => '',
            "phone" => $this->params['phonenumber'],
            "ApplicantNames" => $this->params['name'],
            "ClientPasscode" => 'CLGif1*$pQPyMbfd0',
        );

    }
    public function confirmStatement()
    {
       // dd($this->params);
        $pass = 'CLGif1*$pQPyMbfd0*';
        $curl = curl_init();
        $ticket = $this->params['ticketID'];
        $otp = $this->params['password'];
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://mybankstatement.net/TPServices/webservice.asmx?wsdl=",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\n    <Body>\n        <confirmStatement xmlns=\"http://tempuri.org/\">\n 
            <TicketNO>$ticket</TicketNO>\n   <Password>$otp</Password>\n  <clientID>127</clientID>\n 
            <ClientPasscode>$pass</ClientPasscode>\n  </confirmStatement>\n  </Body>\n</Envelope>",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: text/xml",
                "Postman-Token: 00a9d424-fa34-4ca8-83da-cabc4b02d709",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
           return $err;
        } else {
            return $response;
        }
    }

}
