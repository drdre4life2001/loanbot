<?php

namespace App;



use Illuminate\Database\Eloquent\Model;

class CustomerReport extends Model
{
  
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    

    protected $fillable = [
        'customer_id','started_by', 'completed_by','status', 'analysis_id', 'loan_tenure', 'csv_path', 'bank_code'
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
    public function customer() {
        return $this->belongsTo()()('App\Models\Customer');
    }
    

}