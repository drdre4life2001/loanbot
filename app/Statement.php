<?php

namespace App;



use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
  
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    

    protected $fillable = [
        'customer_id','transaction_date', 'debit','credit', 'balance', 'reference', 'remarks', 'report_id'
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
    public function statement() {
        return $this->belongsTo()('App\Models\Customer');
    }
    

}