<?php

namespace App;



use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    

    protected $fillable = [
        'bvn','first_name', 'last_name','phone', 'bank_id', 'date_of_birth'
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
    public function statement() {
        return $this->hasOne()('App\Models\Statement');
    }
    

}