<?php

namespace App;



use Illuminate\Database\Eloquent\Model;

class StatementSample extends Model
{
  
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    

    protected $fillable = [
        'bank_name','code', 'sample','status'
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
  
    

}