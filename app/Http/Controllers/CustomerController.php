<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerReport;
use App\Helpers\CheckBvn;
use App\Helpers\CRC;
use App\Helpers\GetCSV;
use App\Helpers\MyBankStatement;
use App\Helpers\XdsLoanEngine;
use App\Statement;
use Auth;
//use Gufy\PdfToHtml\Pdf;
use DB;
use function GuzzleHttp\json_decode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SoapClient;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::paginate(15);
        return view('customer.all-customers', compact(['customers']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if ($request->isMethod('post')) {
            $details = $request->all();
            $user_id = uniqid();
            $request->validate([
                'bank' => 'required',
                'bvn' => 'required|digits:11',
                'phone' => 'required',
            ]);

            $bvn = DB::table('customers')->where('bvn', $details['bvn'])->value('bvn');
            if ($bvn) {
                $request->session()->put('msg');
                return back()->withInput()->with('msg', 'Customer already added');
            }

            //check if the customer data exist using the bvn
            if (empty($bvn)) {
                $checkbvn = new CheckBvn;
                $result = $checkbvn->verify($details['bvn']);
                if (array_key_exists('data', $result)) {

                    if ($result['data']['bvn'] === $details['bvn']) {

                        $insertprofile = array(
                            'first_name' => ($result['data']['first_name']),
                            'last_name' => ($result['data']['last_name']),
                            'date_of_birth' => $result['data']['dob'],
                            'unique_id' => $user_id,
                            'phone' => $result['data']['mobile'],
                            'bvn' => $result['data']['bvn'],

                        );

                        $customer = Customer::create($insertprofile);

                    } else {
                        $request->session()->put('msg');
                        return back()->withInput()->with('msg', 'Invalid BVN');
                    }

                }
            }
            $customer_id = DB::table('customers')->where('bvn', $details['bvn'])->value('id');
            //check if xds record exist before creation
            $bank = DB::table('banks')->where('bank_id', $details['bank'])->get();

            //if customer bank is not active in mybankstatement API, upload manually

            if ($is_active == 1) {

                $name = $result['data']['first_name'] . ';' . $result['data']['last_name'];
                $acc = $details['account'];
                $bankid = $details['bank'];
                $start = date('d-M-Y', strtotime('-3 months'));
                // dd($start);
                $end = date('d-M-Y');
                $phonenumber = $result['data']['mobile'];
                $role = 'Applicant';
                $username = $details['email'];
                $country = 'Nigeria';
                //$id = $customer_id;
                $params = array(
                    'name' => $name,
                    'account' => $acc,
                    'bankid' => $bankid,
                    'start' => $start,
                    'end' => $end,
                    'phonenumber' => $phonenumber,
                    'role' => $role,
                    'username' => $username,
                    'country' => $country,
                    'id' => $customer_id,
                );
                //  dd($params);
                // $statement = new MyBankStatement($params);

                if ($statement->sendStatement() == 'success') {
                    return redirect()->route('automatic-statement');
                } elseif ($statement->sendStatement() == 'account already verified') {
                    back()->withErrors('msg', 'This account has aleady been verified');
                }

            } else {

                return view('customer.add-customer-success');
            }

        }

        $banks = DB::table('banks')->get();
        return view('customer.add-customer', compact('banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function analysestatement(Request $request)
    {
        if ($request->isMethod('post')) {

            //dd($request->all());
            if (!is_null($request->bank)) {
                $bank = DB::table('banks')->where('id', $request->bank)->first();
                $bank_short = $bank->bank_code;
            } else {
                $bank_short = null;
            }

            $request->validate([
                // 'statement' => 'required|mimes:pdf',
                'bvn' => 'required',
            ]);

            $customer = DB::table('customers')->where('bvn', $request['bvn'])->first();

            if (is_null($customer)) {
                $user_id = uniqid();
                $checkbvn = new CheckBvn;
                $result = $checkbvn->verify($request['bvn']);
                if (array_key_exists('data', $result)) {

                    if ($result['data']['bvn'] === $request['bvn']) {

                        $insertprofile = array(
                            'first_name' => ($result['data']['first_name']),
                            'last_name' => ($result['data']['last_name']),
                            'date_of_birth' => $result['data']['dob'],
                            'unique_id' => $user_id,
                            'phone' => $result['data']['mobile'],
                            'bvn' => $result['data']['bvn'],
                        );
                        $customer = Customer::create($insertprofile);
                    }

                } else {
                    $request->session()->put('msg');
                    return back()->withInput()->with('msg', 'Invalid BVN');
                }
            }

            $customer_id = DB::table('customers')->where('bvn', $request['bvn'])->value('id');


            if ($request->statement) {

                $details = $request->all();
                $file_type = $request->file('statement')->getClientOriginalExtension();
                $size = $request->file('statement')->getClientSize();
                $file_name = bin2hex(random_bytes(14)).'_'.$customer_id;
                // dd($file_name);
               

                $name = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
                //  $file = Storage::put($file_name,file_get_contents($csv));

                $file = $request->statement->storeAs('public', "$file_name.pdf");
              
                $exists = DB::table('xds_fullreport')->where('x_user_id', $customer_id)->first();
               
                // if(empty($exists)){
                //     $xds = new XdsLoanEngine;
                // $xds_report = $xds->bureauAnalysis($request['bvn'], $customer_id);
                // //dd($xds_report);
                // $response = json_decode($xds_report, true);
                // $crc = new CRC;
                // $crc_report = $crc->creditsummary($request['bvn'], $customer_id);

            //     }else{
            //     $exists = DB::table('xds_fullreport')->where('x_user_id', $customer_id)->first();
            //     $date = strtotime($exists->x_created);
            //     $now = strtotime(date('Y-m-d'));
            //     $lastmonth = strtotime("-1 month", $now);
            //     // if ($date < $lastmonth) {
            //     // $xds = new XdsLoanEngine;
            //     // $xds_report = $xds->bureauAnalysis($request['bvn'], $customer_id);
            //     // //dd($xds_report);
            //     // $response = json_decode($xds_report, true);
            //     // // $crc = new CRC;
            //     // // $crc_report = $crc->creditsummary($request['bvn'], $customer_id);
            //     // }
            // }
            //     //dd($$request['bvn']);
                $pdf = new GetCSV;
                $csv = $pdf->getcsv($customer_id, $file_name, $bank_short);
                $path = pathinfo($csv);
                $csvfile = $path['filename'];

                $report = $this->createtereport($customer_id, $csvfile, $bank_short);
                $test = DB::table('customers')->where('id', $customer_id)->update(array(
                    'pdf_statement' => $file_name,
                ));
                return view('customer.add-customer-success', compact(['customer_id', 'report']));

            } elseif ($request->ticket && $request->otp) {
                $params = [
                    'ticketID' => $request->ticket,
                    'password' => $request->otp,
                    "clientID" => '26',
                    "clientPasscode" => 'gst45W*$SOOx',
                ];

                $client = new SoapClient("https://mybankstatement.net/TPServices/webservice.asmx?wsdl", array('exceptions' => 0));
                //dd($request->all());
                $response = new MyBankStatement($params);

                //dd($response);
                try {
                    $response = $client->confirmStatement($params);
                    $response = $client->GetStatementJSONObject($params);
                    $details = $response->GetStatementJSONObjectResult;
                    $nstatement = json_decode($details, true);
                    $statements = $nstatement['Details'];
                    //dd($statements);
                } catch (\Exception $e) {

                }
                if (isset($statements)) {
                    // dd($nstatement);

                    foreach ($statements as $last) {
                        $data = array(
                            'customer_id' => $customer_id,
                            'transaction_date' => $last['PTransactionDate'],
                            'debit' => $last['PDebit'],
                            'credit' => $last['PCredit'],
                            'balance' => $last['PBalance'],
                            //  'reference' => $last['Reference'],
                            'remarks' => $last['PNarration'],
                        );
                        $inserted = Statement::create($data);

                    }
                    $csvfile = null;

                    $report = $this->createtereport($customer_id, $csvfile, $bank_short);
                    //dd($report);
                    return view('customer.add-customer-success', compact(['customer_id', 'report']));
                } else {
                    return back()->with('msg', 'Statement not ready. Please try again');
                }
                // dd($inserted);
            } else {

                return back()->with('msg', 'Incomplete Input');
            }

            // $x_customer_id = DB::table('customers')->where('bvn', $request['bvn'])->value('id');

            //$crc_report = DB::table('crc_report')->where('customer_id', $customer_id)->value('customer_id');

            // $crc = new CRC;
            // $crc_report = $crc->creditsummary($request['bvn'], $customer_id);
            // dd($crc_report);
        } else {

            return back()->with('msg', 'Incorrect Input');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function automaticstatement(Request $request)
    {
        if ($request->isMethod('post')) {

            $details = $request->all();
            $request->validate([
                'bvn' => 'required',
                'bank' => 'required',
                'account' => 'required',
            ]);

            $customer = DB::table('customers')->where('bvn', $details['bvn'])->first();

            // dd($customer->id);
            $user_id = uniqid();
            if (is_null($customer)) {
                $checkbvn = new CheckBvn;
                $result = $checkbvn->verify($details['bvn']);
                if (array_key_exists('data', $result)) {

                    if ($result['data']['bvn'] === $details['bvn']) {

                        $insertprofile = array(
                            'first_name' => ($result['data']['first_name']),
                            'last_name' => ($result['data']['last_name']),
                            'date_of_birth' => $result['data']['dob'],
                            'unique_id' => $user_id,
                            'phone' => $result['data']['mobile'],
                            'bvn' => $result['data']['bvn'],
                        );
                        $customer = Customer::create($insertprofile);

                    }

                } else {
                    $request->session()->put('msg');
                    return back()->withInput()->with('msg', 'Invalid BVN');
                }
            }
            if ($customer) {
                $phone = DB::table('customers')->where('bvn', $details['bvn'])->value('phone');

                // dd($customer->id);

                $acc = $details['account'];
                $bankid = $details['bank'];
                $start = date('d-M-Y', strtotime('-3 months'));
                $end = date('d-M-Y');
                $phonenumber = $phone;
                $role = 'Applicant';
                $name = $customer->last_name . " " . $customer->first_name;
                $country = 'Nigeria';
                $params = array(
                    'name' => $name,
                    'account' => $acc,
                    'bankid' => $bankid,
                    'start' => $start,
                    'end' => $end,
                    'phonenumber' => $phonenumber,
                    'role' => $role,
                    'country' => $country,
                    'id' => $customer->id,
                );
                $statement = new MyBankStatement($params);
                $customer_id = $customer->id;
                if ($statement->sendStatement() == 'success') {
             
                    $exists = DB::table('xds_fullreport')->where('x_user_id', $customer_id)->first();
                    $date = strtotime($exists->x_created);
                    $now = strtotime(date('Y-m-d'));
                    $lastmonth = strtotime("-1 month", $now);
                    if ($date < $lastmonth) {
                    $xds = new XdsLoanEngine;
                    $xds_report = $xds->bureauAnalysis($request['bvn'], $customer_id);
                    $crc = new CRC;
                    $crc_report = $crc->creditsummary($request['bvn'], $customer_id);
                    }
                    $update = DB::table('customers')->where('id', $customer_id)->update(array(
                        'account_number' => $acc
                    ));

                    return view('customer.customer-ticket', compact(['customer_id']));
                } else {
                    return back()->with('msg', 'Statement could not be initiated. Please try again');
                }

            }

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mybankstatement(Request $request)
    {

        if ($request->isMethod('post')) {

            $customer_id = $request->id;
            $request->validate([
                'ticket' => 'required',
                'otp' => 'required',
            ]);

            $params = [
                'ticketID' => $request->ticket,
                'password' => $request->otp,
                "clientID" => '26',
                "clientPasscode" => 'gst45W*$SOOx',
            ];
            // dd($params);
            $client = new SoapClient("https://mybankstatement.net/TPServices/webservice.asmx?wsdl", array('exceptions' => 0));

            $statements = [1];
            $response = new MyBankStatement($params);

            //dd($response);
           
             
            try {
                $response = $response->confirmStatement();
                $response = $client->GetStatementJSONObject($params);
               // dd($response);
                $details = $response->GetStatementJSONObjectResult;
                $nstatement = json_decode($details, true);
                $statements = $nstatement['Details'];
               

            } catch (\Exception $e) {

            }
            if (count($statements) > 1) {
                // dd($nstatement);
                foreach ($statements as $last) {
                    $data = array(
                        'customer_id' => $customer_id,
                        'transaction_date' => $last['PTransactionDate'],
                        'debit' => $last['PDebit'],
                        'credit' => $last['PCredit'],
                        'balance' => $last['PBalance'],
                        //  'reference' => $last['Reference'],
                        'remarks' => $last['PNarration'],
                    );
                    $inserted = Statement::create($data);

                }
                $csvfile = null;
                $bank_short = null;
                $report = $this->createtereport($customer_id, $csvfile, $bank_short);
                //dd($report);

                return view('customer.add-customer-success', compact(['customer_id','report']));
            } else {
                $request->session()->flash('msg', 'Statement still processing, Please retry');
                return view('customer.customer-ticket')->with('customer_id', $customer_id)->with('msg', 'Invalid credentials');
            }
            // dd($inserted);

        }
        return view('customer.customer-ticket');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createtereport($customer_id, $csvfile, $bank_short)
    {
        $initiated_by = Auth::user()->id;

        $analysis_id = uniqid();
        $report = array(
            'customer_id' => $customer_id,
            'started_by' => $initiated_by,
            'status' => 'Initiated',
            'analysis_id' => $analysis_id,
            'loan_tenure' => 3,
            'csv_path' => $csvfile,
            'bank_code' => $bank_short,

        );
        $analysis = CustomerReport::create($report);

        return $analysis;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleted()
    {
        return View('customer.deleted-customers');
    }

}
