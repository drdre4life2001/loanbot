<?php

namespace App\Http\Controllers;

use App\StatementSample;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StatementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $statements = StatementSample::all();
        $banks = DB::table('banks')->get();
        return view('statement.index', compact('statements', 'banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {

    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            //  dd($request->all());
            $request->validate([
                'bank' => 'required',
                'description' => 'required',
                'statement' => 'required',
            ]);

            $file_name = $request->bank . bin2hex(random_bytes(5));
            // dd($file_name);

            $file = $request->statement->storeAs('public', "$file_name.pdf");
            $report = array(
                'bank_name' => $request->bank,
                'code' => $request->description,
                'status' => 'pending',
                'sample' => $file_name . '.pdf',

            );
            $sample = StatementSample::create($report);
            if (isset($sample)) {
                return back()->with('msg', 'New statement sample added');
            } else {
                return back()->with('msg', 'Statement sample could not be added');

            }
        }
        $banks = DB::table('banks')->get();
        return view('statement.add-statement', compact('banks'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'status' => 'required',
           
            
        ]);

       // dd($request->all());
        $flight = StatementSample::find($request->id);

        $flight->code = $request->description;
        $flight->status = $request->status;
        $flight->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
