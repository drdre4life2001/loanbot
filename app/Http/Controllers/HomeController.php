<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerReport;
use App\Helpers\Filter;
use App\Helpers\LoanCalculator;
use App\Helpers\StatementAnalysis;
use App\Statement;
use App\User;
use App\XDS;
use App\XdsCredit;
use DB;
use Illuminate\Http\Request;
use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //dashboard
        $customers = Customer::all();
        $total_customers = count($customers);
        $banks = DB::table('banks')->where('active', 1)->get();
        $pdf_banks = DB::table('banks')->where('pdf', 1)->get();
        $pdf_analysis = DB::table('customer_reports')->whereNotNull('csv_path')->count();

        $mbs_analysis = DB::table('customer_reports')->whereNull('csv_path')->count();

        // dd($analysis_count);
        return view('dashboard.index', compact(['banks', 'total_customers', 'pdf_banks', 'mbs_analysis', 'pdf_analysis']));
    }

    public function updatecategory(Request $request)
    {
        DB::table('statements')->where('customer_id', $request->customer_id)->where('id', $request->id)->update(array(
            'category' => $request->category,
        ));

        return back();
    }

    public function updatecomment(Request $request)
    {
        // dd($request->all());
        $comment = $request->comment;
        // dd($comment);
        // $report = DB::table('customer_reports')->where('customer_id', $requestcustomer_id)->where('id', $report_id)->first();
        // dd($report);
        DB::table('customer_reports')->where('id', $request->report_id)->update(array(
            'comment' => $comment,
        ));

        return back();
    }

    public function updatetenure(Request $request)
    {
        if ($request->tenure < 1 || $request->tenure > 12) {

            return back()->with('msg', 'Please enter a valid loan tenure');
        } else {
            DB::table('customer_reports')->where('customer_id', $request->customer_id)->update(array(
                'loan_tenure' => $request->tenure,
            ));

            return back();
        }
    }
    public function loancalculator(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'salary1' => 'required',
                'salary2' => 'required',
                'salary3' => 'required',
            ]);
            $details = $request->all();
            // dd($details);
            $salaries = array(
                'salary1' => $details['salary1'],
                'salary2' => $details['salary2'],
                'salary3' => $details['salary3'],
                // 'salary4' => $details['salary4'],
                // 'salary5' => $details['salary5'],
                // 'salary6' => $details['salary6'],
            );
            //   $salaries = array_filter($salary);

            if (count($salaries) >= 3) {
                $netpay = [];
                foreach ($salaries as $salary) {
                    $netpay[] = $salary;
                }
                // dd($netpay);
                $max_netpay = max($netpay);
                $lowest_netpay = min($netpay);
                $total_netpay = array_sum($netpay);
                $pay_count = count($netpay);
                $average_salary = (($total_netpay - $lowest_netpay) - $max_netpay) / ($pay_count - 2);

                // dd(($average_salary));
                $calculations = new LoanCalculator;
                $tenure = $details['tenure'];
                $calculations = $calculations->check($average_salary, $details['repayment'], $tenure);
                //dd($calculations);
                return view('dashboard.calculations', compact(['calculations', 'average_salary']));

            } else {
                // return back()->withInput('msg', 'You must provide at least 3 salary entry');
            }
        }
        return view('dashboard.loan-calculator');
    }

    public function show($customer_id, $report_id)
    {
//         $file = file("http://104.154.164.127/Content/docs/doc634.csv", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
// dd($file);
         
          //  $lines = new Filter();
        //  $final = $lines->scb($file, $customer_id);

        //  dd($file);
        $report = DB::table('customer_reports')->where('customer_id', $customer_id)->where('id', $report_id)->first();
        // dd($report);
        $bank = $report->bank_code;
        $report_id = $report->id;

        // dd($report);
        // $crc_report = DB::table('crc_report')->where('customer_id', $customer_id)->first();
        // $crc_credit_report = DB::table('crc_credit_rating')->where('customer_id', $customer_id)->first();
        // $xds_report = XDS::where('x_user_id', $customer_id)->get();
        $customer = Customer::where('id', $customer_id)->get();
        // dd($customer_id);
        $xds_credit = XdsCredit::where('xc_x_id', $customer_id)->get();

        $csvfile = DB::table('customer_reports')->where('customer_id', $customer_id)->where('id', $report_id)->value('csv_path');

        // dd($csvfile);
        // $results = DB::table("select * from statements where customer_id = $customer_id");
        $results = DB::table('statements')->where('customer_id', $customer_id)->where('report_id', $report->id)->get();
        //  dd($results);
        $file = [1];
        if (count($results) == 0) {
            try {
               // $file = file("http://104.154.164.127/Content/docs/doc9128.csv", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                $file = file("http://104.154.164.127/Content/docs/$csvfile.csv", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

            } catch (\Exception $e) {
            }

            if (count($file) > 1) {

                if (!is_null($bank)) {
                    $lines = new Filter();
                   
                     try{

                         $final = $lines->$bank($file, $customer_id, $report->id);
                     }catch (\Exception $e){
                        return redirect()->route('dashboard')->with('msg', "Incorrect bank and statement combination");

                     }   

                } 

            } else {
                return redirect()->route('dashboard')->with('msg', "Statement analysis not ready. Please retry");

            }
        }
        //dd($result);

        $salaries = [];
        // $average_salary = 0;
        $results = DB::table('statements')->where('customer_id', $customer_id)->where('report_id', $report_id)->get();
        if (count($results) > 1) {
            //dd($results);

            $statementAnalysis = new StatementAnalysis;
            $summary = $statementAnalysis->summary($customer_id, $report_id);
            $analysis = $statementAnalysis->analyse($results, $customer_id, $report_id);

            // dd($analysis);

            // $allrepayments = Statement::where('remarks', 'LIKE', '%' . 'repayment' . '%')->where('debit', '>=', 1)->where('report_id', $report->id)->orwhere('remarks', 'LIKE', '%' . 'credit' . '%')
            //     ->where('debit', '>=', 1)->where('report_id', $report->id)->orwhere('remarks', 'LIKE', '%' . 'loan' . '%')
            //     ->where('debit', '>=', 1)->where('report_id', $report->id)->get();

            $others = DB::table('statements')->where('category', null)->where('report_id', $report_id)->get();
            $results = DB::table('statements')->where('customer_id', $customer_id)->where('report_id', $report_id)->get();
            //dd($results);

            // foreach ($allrepayments as $repayment) {
            //     $update = DB::table('statements')->where('id', $repayment->id)->update(array(
            //         'category' => 'Repayment',
            //     ));
            // }

            foreach ($others as $result) {

                $update = DB::table('statements')->where('id', $result->id)->update(array(
                    'category' => 'Others',
                ));

            }
            //  dd($report);
            $salaries = Statement::where('category', 'Salary')->where('credit', '>', 0)->where
            ('customer_id', $customer_id)->where('report_id', $report->id)->get();

            if (count($salaries) != 0) {

                $netpay = [];
                $dates = [];
                foreach ($salaries as $salary) {
                    $netpay[] = $salary->credit;
                    $dates[] = $salary->transaction_date;
                }

                //  dd($netpay);
                $last_salaray_date = strtotime($dates[0]);
                if (!empty($last_salaray_date)) {
                    $last_salary_date = date('d-M-Y', $last_salaray_date);
                } else {
                    $last_salary_date = $dates[0];
                }

                $max_netpay = max($netpay);
                // dd($max_netpay);
                $lowest_netpay = min($netpay);

                $total_netpay = array_sum($netpay);
                // dd($total_netpay);
                $pay_count = count($netpay);
                $all = (($total_netpay - $lowest_netpay) - $max_netpay);
                $sal = $pay_count - 2;
                if ($all == 0 || $sal == 0) {

                    $average_salary = $lowest_netpay;
                } else {
                    $average_salary = (($total_netpay - $lowest_netpay) - $max_netpay) / ($pay_count - 2);

                }

                $average_salary = round($average_salary, -1);

                $tenure = DB::table('customer_reports')->where('customer_id', $customer_id)->value('loan_tenure');

                $repayments = Statement::where('category', 'Repayment')->where('debit', '>', 1)->
                    where('customer_id', $customer_id)->where('report_id', $report_id)->pluck('debit');
                // dd($repayments);
                $test = json_decode($repayments, true);
                //dd($test);
                $new = array_unique($test);
                //dd($new);
                $repay_sum = array_sum($new);

                $credit_repayment = DB::table('xds_credit')->where('xc_user_id', $customer_id)->where('CurrentBalanceAmt', '>', 1)
                    ->sum('InstalmentAmount');

                if ($repay_sum > $credit_repayment) {

                    $repayment = $repay_sum;
                } else {
                    $repayment = $credit_repayment;

                }

                //get loan calculations
                $calculations = new LoanCalculator;
                $calculations = $calculations->calculate($average_salary, $repayment, $customer_id);
                //  dd($calculations['max_loan_amount']);
                $comment = DB::table('customer_reports')->where('id', $report_id)->value('comment');

                //update loanbot comment
                if ($calculations['max_loan_amount'] < 0 && is_null($comment)) {

                    DB::table('customer_reports')->where('id', $report_id)->update(array(
                        'Comment' => 'Customer loan request cannot be granted. Loan capacity exceeded',
                    ));
                }
                DB::table('customer_reports')->where('id', $customer_id)->update(array(
                    'status' => 'Completed',
                ));

            }

            // pull updated statement
            $results = DB::table('statements')->where('customer_id', $customer_id)->where('report_id', $report->id)->get();
            //pull updated customer report
            $report = DB::table('customer_reports')->where('customer_id', $customer_id)->where('id', $report_id)->first();

        }

        return view('dashboard.credit-analysis', compact(['xds_report', 'report_id', 'report', 'repayment', 'customer_id', 'summary', 'analysis', 'last_salary_date',
            'crc_report', 'crc_credit_report', 'customer', 'xds_credit', 'results', 'average_salary', 'salaries', 'calculations']));
    }

    public function full($customer_id, $report_id)
    {

        $customer = Customer::where('id', $customer_id)->get();
        //get statement
        $results = DB::table('statements')->where('customer_id', $customer_id)->where('report_id', $report_id)->get();
        $statementAnalysis = new StatementAnalysis;
        $summary = $statementAnalysis->summary($customer_id, $report_id);
        $analysis = $statementAnalysis->analyse($results, $customer_id, $report_id);
        $daily_balance = $statementAnalysis->dailybalance($customer_id, $report_id);
        $daily_count = count($daily_balance);
        
        $sumArray = [];
        foreach ($daily_balance as $k => $subArray) {
           $sumArray[] = $subArray->balance;
        }
        //dd($sumArray);
        $filtered = array_filter($sumArray);
        $avg_daily_balance = array_sum($filtered);
        $avg_daily_balance = ceil($avg_daily_balance/$daily_count) ;

        return view('dashboard.full-statement', compact(['customer_id', 'analysis', 'report_id', 'summary',
            'last_salary_date', 'customer', 'xds_credit', 'results', 'avg_daily_balance','daily_balance']));
    }
    // public function calculations($customer_id)
    // {

    //     return view('dashboard.full-statement', compact(['customer_id', 'analysis', 'summary', 'last_salary_date', 'customer', 'xds_credit', 'results']));
    // }

    public function searchCustomer(Request $request)
    {

        $customer_id = Customer::where('bvn', $bvn)->value('id');
        $xds_report = XDS::where('x_user_id', $customer_id)->get();

        $xds_credit = XdsCredit::where('xc_x_id', $customer_id)->get();

        return view('dashboard.credit-analysis', compact(['xds_report', 'customer', 'xds_credit']));
    }

    public function searchbvn(Request $request)
    {
        // dd($request->bvn);

        $bvn = $request->bvn;
        $results =
        $customer_id = Customer::where('bvn', $bvn)->value('id');
        if ($request->isMethod('post')) {
            $report = CustomerReport::where('customer_id', $customer_id)->first();
            //   dd($report);
            if (!is_null($report)) {
                $analysis = CustomerReport::where('customer_id', $customer_id)->get();
                $initiated_by = $report->started_by;
                $initiated_by = User::where('id', $initiated_by)->value('name');
                $completed_by = $report->completed_by;
                $completed_by = User::where('id', $completed_by)->value('name');

                return view('customer.search', compact(['customer_id', 'bvn', 'initiated_by', 'completed_by', 'analysis']));
            } else {
                //dd($report);
                return back()->with('msg', 'No analysis found ');
            }
        }

    }

}
