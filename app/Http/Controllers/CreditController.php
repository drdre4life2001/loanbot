<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Helpers\CheckBvn;
use App\Helpers\XdsLoanEngine;
use App\XDS;
use App\XdsCredit;
use DB;
use Illuminate\Http\Request;

class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {
        if ($request->isMethod('post')) {

            $request->validate([

                'bvn' => 'required|size:11',
            ]);

            $customer = DB::table('customers')->where('bvn', $request['bvn'])->first();

            if (is_null($customer)) {
                $user_id = uniqid();
                $checkbvn = new CheckBvn;
                $result = $checkbvn->verify($request['bvn']);
                if (array_key_exists('data', $result)) {

                    if ($result['data']['bvn'] === $request['bvn']) {

                        $insertprofile = array(
                            'first_name' => ($result['data']['first_name']),
                            'last_name' => ($result['data']['last_name']),
                            'date_of_birth' => $result['data']['dob'],
                            'unique_id' => $user_id,
                            'phone' => $result['data']['mobile'],
                            'bvn' => $result['data']['bvn'],
                        );
                        $customer = Customer::create($insertprofile);
                    }

                } else {
                    return back()->withInput()->with('msg', 'Invalid BVN');
                }
            }

            $customer_id = DB::table('customers')->where('bvn', $request['bvn'])->value('id');

            if (isset($customer_id)) {
                $exists = DB::table('xds_fullreport')->where('x_user_id', $customer_id)->orderBy('x_created', 'desc')->first();
                if (!empty($exists)) {
                    $date = strtotime($exists->x_created);

                    $now = strtotime(date('Y-m-d'));
                    $lastmonth = strtotime("-1 month", $now);
                    if ($date < $lastmonth) {
                        $xds = new XdsLoanEngine;
                        $xds_report = $xds->bureauAnalysis($request['bvn'], $customer_id);
                        //dd($xds_report);
                        $response = json_decode($xds_report, true);
                        // $crc = new CRC;
                        // $crc_report = $crc->creditsummary($request['bvn'], $customer_id);

                    } else {
                        $xds_report = XDS::where('x_user_id', $customer_id)->first();
                        $xds_credit = XdsCredit::where('xc_user_id', $customer_id)->get();
                       // dd($xds_credit);
                        $debt = $xds_report->TotalOutstandingdebt;
                        $xds_report->NoOfOtherAccountsBad;
                        $total_good= $xds_report->NoOfRetailAccountsGood + $xds_report->NoOfOtherAccountsGood;
                        $total_bad = $xds_report->NoOfOtherAccountsBad + $xds_report->NoOfRetailAccountsBad;
                   // dd($total_bad);
                        return view('credit-history.summary', compact(['customer_id', 'xds_report', 'xds_credit', 'total_good', 'total_bad', 'debt']));
                    }

                } else {
                    
                    $xds = new XdsLoanEngine;
                    $xds = $xds->bureauAnalysis($request['bvn'], $customer_id);
                    $xds_report = XDS::where('x_user_id', $customer_id)->first();
                    $xds_credit = XdsCredit::where('xc_x_id', $customer_id)->get();

                    if (empty($xds_report)) {
                        return back()->with('msg', 'No credit history found');
                    } else {
                        $xds_report = XDS::where('x_user_id', $customer_id)->first();
                        $xds_credit = XdsCredit::where('xc_x_id', $customer_id)->get();
                        $debt = $xds_report->TotalOutstandingdebt;
                        $xds_report->NoOfOtherAccountsBad;
                        $total_good= $xds_report->NoOfRetailAccountsGood + $xds_report->NoOfOtherAccountsGood;
                        $total_bad = $xds_report->NoOfOtherAccountsBad + $xds_report->NoOfRetailAccountsBad;
                        return view('credit-history.summary',
                            compact(['customer_id', 'xds_report', 'xds_credit', 'total_good', 'total_bad', 'debt']));

                    }

                }

            } else {
                return back()->with('msg', 'Customer does not exist');
            }

        }
        return view('credit-history.fetch');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
