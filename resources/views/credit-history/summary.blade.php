@extends('layouts.app')

@section('title', 'crdit-history')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text"> Credit History</span>
		<div class="float-right">
			<a href="#" class="btn btn-link">
				<img src="assets/img/icons/printer.png" alt="" class="icon" height="20">
			</a>
			<!-- <a href="#" class="btn btn-primary btn-xl">Update Record</a>
			<a href="#" class="btn btn-danger btn-xl">Delete Record</a> -->
		</div>
	</h2>

	<div class="row">
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">Total Debt</div>
					<div class="title"><b>&#x20A6;{{$debt}}</b> </div>
				
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">Total performing loans</div>
					<div class="title"><b>{{$total_good}}</b> </div>
					
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">Total bad loans</div>
					<div class="title"><b>{{$total_bad}}</b> </div>
					
				</div>
			</div>
		</div>
	</div>

	

	<div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">
						<span>CRC RECORD  <small>(₦)</small></span>
						<span class="float-right">
							<a href="#" class="print btn btn-link">
								<img src="{{ asset('img/icons/printer.png') }}" alt="Print" height="25">
							</a>
							<a href="#collapse-1" class="collapse-toggle d-inline-block" data-toggle="collapse">
								<span class="mdi mdi-chevron-down"></span>
							</a>
						</span>
					</div>
				</div>
				<div class="card-body collapse show" id="collapse-1">
					<table class="table table-borderless">
						<thead>
                        <tr>
								<th>Subscriber Name</th>
								<th>Account Number</th>
								<th>Outstanding Balance</th>
								<th>Installment Amount</th>
								<th>Arrear Amount</th>
								<th>Facility Classification</th>
								<th>Account Status</th>
							</tr>
						</thead>
						<tbody>
                        <tr> 
                        @foreach($xds_credit as $credit)
								<td>{{$credit->SubscriberName}}</td>
								<td>{{$credit->AccountNo}}</td>
									
								<td>{{$credit->CurrentBalanceAmt}}</td>
								<td>{{$credit->InstalmentAmount}}</td>
								<td>1</td>
								<td>{{$credit->PerformanceStatus}}</td>
								<td>{{$credit->AccountStatus}}</td>
							</tr>
				         @endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	
	

	

@stop