@extends('layouts.app')

@section('title', 'Add A Customer')

@section('content')

	<div class="page-head">
		<h2 class="page-head-title clearfix">
			<span class="text">Add Sataement</span>
		</h2
		@if ($errors->any())
  
        <ul>
			<div style="color:red">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
			@endforeach
			</div>
        </ul>
        @endif	
		@if(Session::has('msg'))
       <div style="color:red">
        <h6>{{ Session::get('msg') }}</h6>
    </div>
@endif

	</div>
	
	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-body p-5">
				<form method="POST" action="{{ route('add-customer') }}">
						@csrf
					
					<div class="form-group">
						<label for="phone_no"  class="form-label">Phone Number</label>
						<input type="number" name="phone" class="form-control" id="account" placeholder="0011223344" required>
					</div>
					<div class="form-group">
						<label for="bvn" class="form-label">Bank</label>
						
						
						<select type="text" name="bank" class="form-control"  id="bvn" placeholder="2323233234" required>
						@foreach($banks as $bank)
							<option  value="{{$bank->bank_id}}">{{$bank->bank_name}}</option>
							@endforeach
						</select>
					   
					</div>
				

					<div class="form-group">
						<div class="row">
							<div class="col-6">
								<button type="submit" class="btn btn-primary btn-xl btn-block" style="border-radius: 3px">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>						
		</div>
		<div class="col-md-5">
			<h2 class="font-weight-bold mb-4">Additional Information</h2>

			<p class="text-muted" style="font-size: 15px">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur optio quae accusamus eius aperiam dolore, non, totam quia aut pariatur recusandae facere quisquam?
			</p>

		</div>
	</div>

@stop