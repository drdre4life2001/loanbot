@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

	<div class="page-head">
		<h2 class="page-head-title clearfix">
			<!-- <span class="text">Customer Report</span> -->
		</h2>
	</div>
	
	<div class="row">
		<div class="col-md-10">
			<div class="card">
				<div class="card-body p-5">
					
					<img src="{{ asset('/img/icons/checkmark-green.png') }}" alt="" class="img-fluid d-block mx-auto mb-5 mt-3" style="width: 100px">

					<h4 class="text-center mb-5 mt-5 font-weight-bold">Account Statement Added Successfully</h4>

					<div class="form-group">
						<div class="row">
							<div class="col-6 m-auto">
								<a href="{{ route('credit-analysis',[$customer_id, $report->id])}}">
								<button type="submit" class="btn btn-primary btn-xl btn-block"  style="border-radius: 3px">Proceed to Report</button>
								</a>
							</div>
						</div>
					</div>

				</div>
			</div>						
		</div>
		<!-- <div class="col-md-5">
			<h2 class="font-weight-bold mb-4">Additional Information</h2>

			<p class="text-muted" style="font-size: 15px">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur optio quae accusamus eius aperiam dolore, non, totam quia aut pariatur recusandae facere quisquam?
			</p>

		</div> -->
	</div>
	
@stop