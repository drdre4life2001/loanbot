@extends('layouts.app')

@section('title', 'All Customers')

@section('content')

	<!-- <h2 class="page-title clearfix">
		<span class="text">Data Overview</span>
		<a href="/add-customer" class="btn btn-primary float-right btn-xl">Create Customer</a>
	</h2> -->

	<div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">All Customers</div>
				</div>
				<div class="card-body">
					<table class="table table-borderless">
						<thead>
							<tr>
								<th>Name</th>
								<th>BVN</th>
								<th>Date Of Birth</th>
								<th>Phone Number</th>
								
							</tr>
						</thead>
						<tbody>
						@foreach($customers as $customer)
							<tr>
								<td><b>{{$customer->first_name}} {{$customer->last_name}}</b></td>
								<td>{{$customer->bvn}}</td>
								<td>{{$customer->date_of_birth}}</td>
								<td>{{$customer->phone}}</td>
							
								
								<!-- <td>
									<a href="{{ url('credit-analysis', $customer->id)}}">
									<button class="btn btn-primary" >View Analysis</button>	
									<a>
							    </td> -->
							</tr>
							@endforeach
							{{ $customers->links() }}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop