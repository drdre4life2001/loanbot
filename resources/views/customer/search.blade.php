@extends('layouts.app')

@section('title', 'Search')

@section('content')

	<h2 class="page-title clearfix">
	@if(is_null($analysis))
		<span class="text">No credit analysis result for <b>{{$bvn}}</b></span>
	@endif
	
	</h2>

	<div class="row" style="margin-bottom: 8rem">
		<div class="col-12 col-md-12">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="widget-chart-container">
						<table class="table table-borderless">
						

						
							<thead>
								<tr>
									<th>Date</th>
									<th>Initiated By</th>
									<th>Completed By</th>
									<th>Bank</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
									@foreach($analysis as $result)
								<tr>
									<td>{{$result->created_at}}</td>
									<td>{{$initiated_by}}</td>
									<td>{{$completed_by}}</td>
									<td>{{$result->bank_code}}</td>
										<td><span class="label bg-success text-white">{{$result->status}}</span></td>
									<td><a href="{{ route('credit-analysis',[$customer_id, $result->id])}}" class="btn btn-primary btn-sm btn-block btn-round">View Analysis</a></td>
								</tr>
							</tbody>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop