@extends('layouts.app')

@section('title', 'Add Customer Ticket Number')

@section('content')

	<div class="page-head">
	@if(Session::has('msg'))
<div class="alert alert-danger alert-dismissible text-center col-md-6 close" data-dismiss="alert">
<p><h4>{{ Session::get('msg') }}  ×</h4></p>
</div>
@endif
		<h2 class="page-head-title clearfix">
			<span class="text">Account Statement Information</span>

		</h2>
	</div>

	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-body p-5">

					
					<form method="POST" action="{{route('automatic-statement')}}">
						@csrf
					<div class="form-group">
					
						<label for="bvn" class="form-label ">Ticket Number</label>
						<input type="text" name="ticket" class="form-control" id="ticket" placeholder="Ticket Number" required>
					</div>

					<div class="form-group">
						<label for="phone_no" class="form-label">OTP</label>
						<input type="number" name="otp" class="form-control" d="phone_no" placeholder="OTP" required>
					</div>
					<input type="hidden" name="id" class="form-control" d="phone_no" value="{{$customer_id}}">

					<div class="form-group">
						<div class="row">
							<div class="col-6">
								<button type="submit" class="btn btn-primary btn-xl btn-block" style="border-radius: 3px">Submit</button>
							</div>
						</div>
					</div>

				</div>
			</div>						
		</div>
		<div class="col-md-5">
			<h2 class="font-weight-bold mb-4">Additional Information</h2>

			<p class="text-muted" style="font-size: 15px">
				Input the ticket number and pass code as received by the customer to access statement 
			</p>

		</div>
	</div>

@stop