
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from foxythemes.net/preview/products/beagle/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Jan 2019 09:56:51 GMT -->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="{{ asset('/img/logo-fav.png') }}">
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/lib/jqvmap/jqvmap.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
	<link rel="stylesheet" href="{{ asset('/css/app.css') }}" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('/css/main.css') }}" type="text/css"/>
</head>
<body>
	
	<div class="be-wrapper be-fixed-sidebar">
		<nav class="navbar navbar-expand fixed-top be-top-header">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="{{('/'	)}}">
						<img src="{{ asset('/img/logo_color.png') }}" alt="" height="40">
					</a>
				</div>
				<div class="be-right-navbar">
			
				<form method="POST" action="{{ route('search') }} " enctype="multipart/form-data">
			    	@csrf
					<div class="form-group search-form">
				
						<div class="input-group">
							<input type="text" name="bvn" class="form-control" placeholder="Search Customer With Bvn" required>
							<span class="input-group-append">
								<button class="btn" type="submit">
									<span class="icon mdi mdi-search"></span>
								</button>
							</span>
						</div>
					</div>


						</form>
					<ul class="nav navbar-nav ml-auto be-user-nav">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
								<img src="{{ asset('/img/avatar.png') }}" alt="Avatar">
								<span class="user-name d-inline">
									<span class="font-weight-bold">{{ Auth::user()->name}}</span>
								</span>
							</a>
						</li>
					</ul>
					<ul class="nav navbar-nav">

						<li class="nav-item">
							<a href="#" class="nav-link">
								<img src="{{ asset('img/icons/home-icon-silhouette.png') }}" class="icon" height="25">
							</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link">
								<img src="{{ asset('img/icons/cogwheel-outline.png') }}" class="icon" height="25">
							</a>
						</li>
						<li class="nav-item">
							<a href="{{route('logout')}}" class="nav-link">
							<span class="font-weight-bold">Logout</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="be-left-sidebar">
			<div class="left-sidebar-wrapper">
				<form class="d-block d-md-none">
					<div class="search-form">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search Customer With Bvn">
							<span class="input-group-append">
								<button class="btn">
									<span class="icon mdi mdi-search"></span>
								</button>
							</span>
						</div>
					</div>
				</form>
				<a class="left-sidebar-toggle" href="#">Dashboard</a>
				
				<div class="left-sidebar-spacer">
					<div class="left-sidebar-scroll">
						<div class="left-sidebar-content">
							<ul class="sidebar-elements">
								<li>
									<a href="{{ url('/') }}" class="active">
										<img src="{{ asset('/img/icons/001-dashboard.png') }}" alt="" class="icon">
										<span>Dashboard</span>
									</a>
								</li>
								
								<li>
									<a href="{{route('loan-calculator')}}">
										<img src="{{ asset('/img/icons/006-budget.png') }}" alt="" class="icon">
										<span>Credit Limit Generator</span>
									</a>
								</li>
								<li>
									<a href="{{route('fetch-history')}}">
										<img src="{{ asset('/img/icons/006-budget.png') }}" alt="" class="icon">
										<span>Fetch Credit history</span>
									</a>
								</li>
								<li>
									<a href="{{route('statement-sample')}}">
										<img src="{{ asset('/img/icons/003-team.png') }}" alt="" class="icon">
										<span> View Sample statements</span>
									</a>
								</li>
								<li>
									<a href="{{route('create-sample')}}">
										<img src="{{ asset('/img/icons/003-team.png') }}" alt="" class="icon">
										<span> Add new statement</span>
									</a>
								</li>
								<li>
									<a href="{{route('all-customers')}}">
										<img src="{{ asset('/img/icons/003-team.png') }}" alt="" class="icon">
										<span> All Customers</span>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="be-content">
			<div class="be-main container-fluid">

				@yield('content')

				<!-- <div class="text-right p-5">
					 <img src="{{ asset('img/logo_color.png') }}" alt="CredPal" height="20">
				</div> -->

			</div>
		</div>
	</div>
	<script src="{{ asset('/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/lib/perfect-scrollbar/js/perfect-scrollbar.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/lib/countup/countUp.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/lib/chartjs/Chart.min.js') }}" type="text/javascript"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>

	<script type="text/javascript">

		$(document).ready(function(){
			//-initialize the javascript
			App.init();
	      	// App.dashboard();

	      	$('.collapse').on('show.bs.collapse hide.bs.collapse', function(event) {
	      		var toggle = $('.collapse-toggle').filter(`[href="#${this.id}"]`);
	      		console.log(toggle)
	      		if (event.type == 'show') {

	      		}else {

	      		}
	      	});

			$('canvas').each(function(a, canvas) {

				ctx = canvas.getContext('2d');
				type = $(canvas).data('type');
				data = $(canvas).data('data');
				width = $(canvas).parent().width();
				ctx.canvas.width = width;

				if (!data) {
					switch (type) {
						case 'bar':
							data = {
								labels: ['', '', '', '', '', '', '', '', '', ''],
								datasets: [
									{
										backgroundColor: '#007bff',
										data: [12, 11, 10, 13, 12, 10, 9, 12, 9, 13, 12]
									},
									{
										backgroundColor: '#95B9E7',
										data: [10, 10, 13, 11, 13, 12, 11, 14, 11, 15, 14]
									}
								],
							};
							break;
						case 'doughnut':
							data = {
								labels: ['Red', 'Yellow', 'blue'],
								datasets: [
									{
										backgroundColor: ['red', 'blue'],
										data: [35, 40]
									}
								]
							}
							break
						default:

							break;
					}
				}

				var chart = new Chart(ctx, {
					type: type,
					data: data,
					options: {
						responsive: false,
						legend: {
							display: false,
						},
						title: {
							display: false,
						},
						scales: {
							xAxes: [{
								display: false,
							}],
							yAxes: [{
								display: false,
							}]
						},
						cutoutPercentage: 75
					}
				});

			});

		});
	</script>

	@stack('js')

</body>

<!-- Mirrored from foxythemes.net/preview/products/beagle/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Jan 2019 09:56:51 GMT -->
</html>
