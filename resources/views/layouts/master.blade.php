<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="{{ asset('/img/logo-fav.png') }}">
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
	<link rel="stylesheet" href="{{ asset('/css/app.css') }}" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('/css/main.css') }}" type="text/css"/>
</head>
<body class="be-splash-screen">
	<div class="be-wrapper">

		<div class="navbar navbar-expanded fixed-top">
			<a href="#" class="navbar-brand">
				<img src="{{ asset('/img/logo_color.png') }}" alt="Credit Direct" height="30">
			</a>
		</div>
		
		<div class="be-content">

			@yield('content')

		</div>

	</div>
	<script src="{{ asset('/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/lib/perfect-scrollbar/js/perfect-scrollbar.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			//-initialize the javascript
			App.init();
		});
		
	</script>
</body>
</html>