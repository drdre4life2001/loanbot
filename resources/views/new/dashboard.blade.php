@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text">Dashboard</span>
	</h2>

	<div class="row" style="margin-bottom: 8rem">
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<h1 class="font-weight-normal text-muted">100</h1>
								<p>Total Clients</p>
							</div>
							<div class="col-5 flex-center">
								<img src="{{ asset('img/icons/person-red.png') }}" alt="Total Clients" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<h1 class="font-weight-normal text-muted">100</h1>
								<p>Total Analyzed</p>
							</div>
							<div class="col-5 flex-center">
								<img src="{{ asset('img/icons/002-analytics.png') }}" alt="Total Analyzed" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<h1 class="font-weight-normal text-muted">100</h1>
								<p>Open Cases</p>
							</div>
							<div class="col-5 flex-center">
								<img src="{{ asset('img/icons/book.png') }}" alt="Open Cases" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="px-1">
				<ul class="nav nav-tabs">
					<li class="nav-item"><a href="#generate" data-toggle="tab" class="nav-link active">Generate Satement</a></li>
					<li class="nav-item"><a href="#analyze" data-toggle="tab" class="nav-link">Analyze Report</a></li>
				</ul>
			</div>
			<div class="tab-content position-relative" style="z-index: 1">
				<div class="tab-pane fade active in show" id="generate">
					<div class="card">
						<div class="card-header">
							<div class="card-title">Generate Statement Token</div>
						</div>
						<div class="card-body p-5">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="bvn">BVN</label>
										<input type="text" class="form-control" id="bvn" placeholder="BVN">
									</div>
									<div class="form-group">
										<label for="bank">BANK</label>
										<select type="text" class="form-control" id="bank">
											<option>Choose Your Bank</option>
										</select>
									</div>
									<div class="form-group mb-5 pb-2">
										<label for="account_no">ACCOUNT NUMBER</label>
										<input type="text" class="form-control" id="account_no" placeholder="ACCOUNT NUMBER">
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-xl btn-primary btn-wide btn-radius">Generate Statement</button>
									</div>
								</div>
								<div class="col-md-5 offset-md-1 d-flex align-items-center">
									<img src="{{ asset('img/icons/statement.png') }}" alt="" class="img-fluid">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="analyze">
					<div class="card">
						<div class="card-header">
							<div class="card-title">Analyze Report</div>
						</div>
						<div class="card-body p-5">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="bvn">BVN</label>
										<input type="text" class="form-control" id="bvn" placeholder="BVN">
									</div>
									<div class="form-group">
										<label for="token">TOKEN</label>
										<input type="text" class="form-control" id="token" placeholder="TOKEN">
									</div>
									<div class="form-group mb-5 pb-2">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input">
											<span class="custom-control-label">I don't have a token</span>
										</label>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-xl btn-primary btn-wide btn-radius">Analyze</button>
									</div>
								</div>
								<div class="col-md-5 offset-md-1 d-flex align-items-center">
									<img src="{{ asset('img/icons/pdf.png') }}" alt="" class="img-fluid">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop