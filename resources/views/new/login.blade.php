@extends('layouts.master')

@section('title', 'Login')

@section('content')

	<div class="main-content container">
		<div class="row" style="min-height: 100vh">
			<div class="col-12 col-md-6 d-flex flex-column align-items-center justify-content-center">
				<form class="w-100">
					<div class="login-form">
						<div class="form-group">
							<label for="email" class="form-label">Email</label>
							<input class="form-control" id="email" type="text" autocomplete="off">
						</div>
						<div class="form-group">
							<label for="password" class="form-label">Password</label>
							<input class="form-control" id="password" type="password">
						</div>
						<div class="form-group row login-tools">
							<div class="col-12 login-remember">
							<label class="custom-control custom-checkbox">
								<input class="custom-control-input" type="checkbox">
								<span class="custom-control-label">Keep Me Signed In</span>
							</label>
							</div>
						</div>
						<div class="form-group row login-submit">
							<div class="col-12 col-md-6">
								<a class="btn btn-primary btn-xl" href="index-2.html" data-dismiss="modal">Login</a>
							</div>
							<div class="col-12 col-md-6 login-forgot-password">
								<a href="forgot-password.html" class="btn btn-default btn-xl">Forgot Password?</a>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-12 offset-md-1 col-md-5 d-flex flex-column align-items-center justify-content-center">
				<img src="{{ asset('/img/tony bot.png') }}" alt="" class="img-fluid">
			</div>
		</div>
	</div>

@stop