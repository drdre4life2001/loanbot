@extends('layouts.app')

@section('title', 'Search')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text">Search result for <b>223344556677</b></span>
	</h2>

	<div class="row" style="margin-bottom: 8rem">
		<div class="col-12 col-md-12">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="widget-chart-container">
						<table class="table table-borderless">
							<thead>
								<tr>
									<th>Date</th>
									<th>Initiated By</th>
									<th>Completed By</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>12/2/2012</td>
									<td>Haruna Ahmadu</td>
									<td>Haruna Ahmadu</td>
									<td><span class="label bg-success text-white">Open</span></td>
									<td><a href="#" class="btn btn-primary btn-sm btn-block btn-round">View Analysis</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop