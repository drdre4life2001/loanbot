@extends('layouts.app')

@section('title', 'Haruna\' Credit Analysis')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text">Haruna's Credit Analysis</span>
		<div class="float-right">
			<a href="#" class="btn btn-link">
				<img src="assets/img/icons/printer.png" alt="" class="icon" height="20">
			</a>
			<a href="#" class="btn btn-primary btn-xl">Update Record</a>
			<a href="#" class="btn btn-danger btn-xl">Delete Record</a>
		</div>
	</h2>

	<div class="row">
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">General Results</div>
					<div class="title"><b>9400</b> <small>Customers</small></div>
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<canvas height="100" class="bar-chart" data-type="bar" width="90">
									Your Browser Cannot Display This Chart
								</canvas>
							</div>
							<div class="col-5">
								<canvas height="100" class="doughnut-chart" data-type="doughnut"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">General Results</div>
					<div class="title"><b>9400</b> <small>Customers</small></div>
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<canvas height="100" class="bar-chart" data-type="bar" width="90">
									Your Browser Cannot Display This Chart
								</canvas>
							</div>
							<div class="col-5">
								<canvas height="100" class="doughnut-chart" data-type="doughnut"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">General Results</div>
					<div class="title"><b>9400</b> <small>Customers</small></div>
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<canvas height="100" class="bar-chart" data-type="bar" width="90">
									Your Browser Cannot Display This Chart
								</canvas>
							</div>
							<div class="col-5">
								<canvas height="100" class="doughnut-chart" data-type="doughnut"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-0 mb-md-4" style="font-size: 15px">
		<div class="col-12 col-md-3">
			<div class="card mb-4 mb-md-0 h-100">
				<div class="card-header">
					<div class="card-sub-title">BVN Details</div>
				</div>
				<div class="card-body">
					<p>Haruna Ahmad Bashiru</p>
					<p>07034776388</p>
					<p>14-OCT-2014</p>
					<p>226363636</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-3">
			<div class="card mb-4 mb-md-0 h-100">
				<div class="card-header">
					<div class="card-sub-title">Credpal Credit Decision</div>
				</div>
				<div class="card-body">
					<p>Credit Limit <b>N20,000</b></p>
					<h2 class="text-center">
						<img src="assets/img/icons/checkmark-green.png" alt="" class="d-inline" height="40">
						<span>Good</span>
					</h2>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6">
			<div class="card mb-4 mb-md-0 h-100">
				<div class="card-header">
					<div class="card-sub-title">&nbsp;</div>
				</div>
				<div class="card-body">
					<div class="row text-center">
						<div class="col-6 border-right">
							<p>Available Balance <b>N500,000</b></p>
							<p>Salary Date <b>30th</b></p>
							<div class="text-center">
								<a href="#comment-modal" data-toggle="modal" class="btn btn-primary btn-xl" style="min-width: 150px; max-width: 100%">Good</a>
							</div>
						</div>
						<div class="col-6">
							<p>Repayments <b>N100,000</b></p>
							<p>Average Salary <b>120,000</b></p>
							<div class="text-center">
								<a href="#comment-modal" data-toggle="modal" class="btn btn-danger btn-xl" style="min-width: 150px; max-width: 100%">Bad</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">
						<span>CRC RECORD ON HARUNA <small>(₦)</small></span>
						<span class="float-right">
							<a href="#" class="print btn btn-link">
								<img src="{{ asset('img/icons/printer.png') }}" alt="Print" height="25">
							</a>
							<a href="#collapse-1" class="collapse-toggle d-inline-block" data-toggle="collapse">
								<span class="mdi mdi-chevron-down"></span>
							</a>
						</span>
					</div>
				</div>
				<div class="card-body collapse show" id="collapse-1">
					<table class="table table-borderless">
						<thead>
							<tr>
								<th>Subscriber Name</th>
								<th>Account Number</th>
								<th>Available Limit</th>
								<th>Outstanding Balance</th>
								<th>Installment Amount</th>
								<th>Arrear Amount</th>
								<th>Facility Classification</th>
								<th>Account Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td></td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td></td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">
						<span>XDS RECORD ON HARUNA <small>(₦)</small></span>
						<span class="float-right">
							<a href="#" class="print btn btn-link">
								<img src="{{ asset('img/icons/printer.png') }}" alt="Print" height="25">
							</a>
							<a href="#collapse-2" class="collapse-toggle d-inline-block" data-toggle="collapse">
								<span class="mdi mdi-chevron-down"></span>
							</a>
						</span>
					</div>
				</div>
				<div class="card-body collapse show" id="collapse-2">
					<table class="table table-borderless">
						<thead>
							<tr>
								<th>Subscriber Name</th>
								<th>Account Number</th>
								<th>Available Limit</th>
								<th>Outstanding Balance</th>
								<th>Installment Amount</th>
								<th>Arrear Amount</th>
								<th>Facility Classification</th>
								<th>Account Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td></td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td></td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">
						<span>CRS RECORD ON HARUNA <small>(₦)</small></span>
						<span class="float-right">
							<a href="#" class="print btn btn-link">
								<img src="{{ asset('img/icons/printer.png') }}" alt="Print" height="25">
							</a>
							<a href="#collapse-3" class="collapse-toggle d-inline-block" data-toggle="collapse">
								<span class="mdi mdi-chevron-down"></span>
							</a>
						</span>
					</div>
				</div>
				<div class="card-body collapse show" id="collapse-3">
					<table class="table table-borderless">
						<thead>
							<tr>
								<th>Subscriber Name</th>
								<th>Account Number</th>
								<th>Available Limit</th>
								<th>Outstanding Balance</th>
								<th>Installment Amount</th>
								<th>Arrear Amount</th>
								<th>Facility Classification</th>
								<th>Account Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td></td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td></td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td></td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
							<tr>
								<td>One Finance</td>
								<td>302221454547</td>
								<td>25,000</td>
								<td>0.00</td>
								<td>16,250.00</td>
								<td></td>
								<td>Performing</td>
								<td>Closed</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="card card-table">
		<div class="card-header">
			<div class="card-title">
				<span>ACCOUNT SUMMARY</span>
				<span class="float-right">
					<a href="#" class="print btn btn-link">
						<img src="{{ asset('img/icons/printer.png') }}" alt="Print" height="25">
					</a>
					<a href="#collapse-5" class="collapse-toggle d-inline-block" data-toggle="collapse">
						<span class="mdi mdi-chevron-down"></span>
					</a>
				</span>
			</div>
		</div>
		<div class="card-body collapse show" id="collapse-5">
			<table class="table table-borderless table-condensed" style="text-align: left;">
				<tbody>
					<tr>
						<td>Opening Balance</td>
						<td>N1,334</td>
						<td><span class="mdi mdi-caret-up text-success" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Closing Balance</td>
						<td>N2,339</td>
						<td><span class="mdi mdi-caret-down text-danger" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Total Credit</td>
						<td>N3,343,433</td>
						<td><span class="mdi mdi-caret-up text-success" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Total Debit</td>
						<td>N1,334</td>
						<td><span class="mdi mdi-caret-down text-danger" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Maximum Balance</td>
						<td>N34,495</td>
						<td><span class="mdi mdi-caret-down text-dark" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Average Balance</td>
						<td>N492</td>
						<td><span class="mdi mdi-caret-up text-success" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Minimum Balance</td>
						<td>N99,000</td>
						<td><span class="mdi mdi-caret-up text-success" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Ratio Of Days In Overdraft</td>
						<td>0</td>
						<td><span class="mdi mdi-caret-down text-dark" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Overdraft</td>
						<td>No</td>
						<td><span class="mdi mdi-caret-down text-dark" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Total Number Of Days In Overdraft</td>
						<td>0%</td>
						<td><span class="mdi mdi-caret-down text-dark" style="font-size: 20px"></span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="card card-table">
		<div class="card-header">
			<div class="card-title">
				<span>ACCOUNT STATEMENT</span>
				<span class="float-right">
					<a href="#" class="print btn btn-link">
						<img src="{{ asset('img/icons/printer.png') }}" alt="Print" height="25">
					</a>
					<a href="#collapse-4" class="collapse-toggle d-inline-block" data-toggle="collapse">
						<span class="mdi mdi-chevron-down"></span>
					</a>
				</span>
			</div>
		</div>
		<div class="card-body collapse show" id="collapse-4">
			<table class="table table-borderless table-condensed">
				<thead>
					<tr>
						<th>Value Date</th>
						<th>Debit</th>
						<th>Credit</th>
						<th>Balance</th>
						<th>Remarks</th>
						<th>Category</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td>May 31, 2018</td>
						<td>305</td>
						<td>0</td>
						<td>1,334</td>
						<td>MNTHLY MTCE FEE</td>
						<td>Charges</td>
						<td>
							<div class="dropleft">
								<a href="#" class="btn btn-sm btn-light btn-round" data-toggle="dropdown">Select One</a>
								<ul class="dropdown-menu text-center">
									<a href="#" class="dropdown-item">Salary</a>
									<a href="#" class="dropdown-item">Charges</a>
									<a href="#" class="dropdown-item">POS</a>
									<a href="#" class="dropdown-item">ATM</a>
									<a href="#" class="dropdown-item">Transfer</a>
									<a href="#" class="dropdown-item">Gamble</a>
								</ul>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="text-center pb-4">
				<a href="#" class="btn btn-primary" style="border-radius: 3px; min-width: 200px">View Full Statements</a>
			</div>
		</div>
	</div>
	
	<div class="card">
		<div class="card-header"></div>
		<div class="card-body">
			<div class="row">
				
				<div class="col-4">
					<div class="text-center mb-5">
						<img src="{{ asset('img/icons/pdf.png') }}" alt="" class="img-fluid" width="200">
					</div>
					<div class="text-center">
						<a href="#">Download Haruna's Account Statement</a>
					</div>
				</div>
				<div class="col-4 d-flex align-items-center justify-center">
					<a href="" class="btn btn-primary" style="border-radius: 3px">Analyze Account Statement</a>
				</div>

			</div>						
		</div>
	</div>

@stop