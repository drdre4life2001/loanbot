@extends('layouts.master')

@section('title', 'Forgot Password')

@section('content')

	<div class="main-content container">
		<div class="row" style="min-height: 100vh">
			<div class="col-12 col-md-6 d-flex flex-column align-items-center justify-content-center">
				<form class="w-100">
					<div class="login-form">
						<div class="form-group">
							<label for="email" class="form-label">Email</label>
							<input class="form-control" id="email" type="text"autocomplete="off">
						</div>
						<div class="form-group row">
							<div class="col-12 col-md-6">
								<a class="btn btn-primary btn-block btn-xl" href="reset-password.html">Submit</a>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-12 offset-md-1 col-md-5 d-flex flex-column align-items-center justify-content-center">
				<img src="{{ asset('/img/tony bot.png') }}" alt="" class="img-fluid">
			</div>
		</div>
	</div>

@stop