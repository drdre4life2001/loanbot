@extends('layouts.master')

@section('title', 'Loan Bot')

@section('content')

	<div class="main-content container py-0 landing-page">
		<div class="row" style="min-height: 100vh">
			<div class="col-12 col-md-6 d-flex flex-column align-items-center justify-content-center">
				<div class="w-100">
					<h1>Loan Bot</h1>
					<p>
						Login to your loan bot dashboard to manage customer loan and to check credit history
					</p>
					<a href="{{ route('login') }}" class="btn btn-primary">Get Started</a>
				</div>
			</div>
			<div class="col-12 offset-md-1 col-md-5 d-flex flex-column align-items-center justify-content-center">
				<img src="{{ asset('/img/tony bot.png') }}" alt="" class="img-fluid">
			</div>
		</div>
	</div>

@stop