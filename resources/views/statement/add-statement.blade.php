@extends('layouts.app')

@section('title', 'Add A Statemet')

@section('content')

	<div class="page-head">

	@if(Session::has('msg'))
        <div class="alert alert-danger alert-dismissible text-center col-md-6 close" data-dismiss="alert">
      <p>{{ Session::get('msg') }}  ×</p>
</div>
@endif 
		<h2 class="page-head-title clearfix">
			<span class="text">Add Sataement</span>
		</h2
		@if ($errors->any())
  
        <ul>
			<div style="color:red">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
			@endforeach
			</div>
        </ul>
        @endif	
		

	</div>
	
	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-body p-5">
				<form method="POST" action="{{ route('create-sample') }}" enctype="multipart/form-data">
						@csrf
					
					
					<div class="form-group">
						<label for="bvn" class="form-label">Bank</label>
						
						
						<select type="text" name="bank" class="form-control"  id="bvn" placeholder="" required>
						<option value="">Choose Bank</option>
						@foreach($banks as $bank)
							<option  value="{{$bank->bank_name}}">{{$bank->bank_name}}</option>
							@endforeach
						</select>
					   
                    </div>
                 
					<div class="form-group">
						<label for="description"  class="form-label">Description <small>Short bank description eg gtb</small></label>
						<input type="text" name="description" class="form-control" id="account" placeholder="" required>
					</div>
				
					<div class="form-group">
						<label for="phone_no"  class="form-label">Statement</label>
						<input type="file" name="statement" class="form-control" id="account" placeholder="" required>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-6">
								<button type="submit" class="btn btn-primary btn-xl btn-block" style="border-radius: 3px">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>						
		</div>
		<div class="col-md-5">
			<h2 class="font-weight-bold mb-4">Additional Information</h2>

			<p class="text-muted" style="font-size: 15px">
				New bank staements can e added here. A short description of the bank to be added eg stanbic.You are required to view existing statement samples before adding new one
			</p>

		</div>
	</div>

@stop