@extends('layouts.app')

@section('title', 'All Customers')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text">Sample Statements</span>
		<a href="{{ route('create-sample')}}" class="btn btn-primary float-right btn-xl">Add new Statement</a>
	</h2>

	<div class="row">
		<div class="col-12">
			<div class="card card-table">
				<!-- <div class="card-header">
					<div class="card-title">All sample statements</div>
				</div> -->
				<div class="card-body">
					<table class="table table-borderless">
						<thead>
							<tr>
								<th>Bank Name</th>
								<th>Statement code</th>
                                <th>Status</th>
                                <th>Date Created</th>
                                <th>Date Updated</th>

							</tr>
			    			</thead>
						<tbody>
						@foreach($statements as $statement)
							<tr>
								<td><b>{{$statement->bank_name}}</b></td>
								<td>{{$statement->code}}</td>
                                <td>{{$statement->status}}</td>
                                <td>{{$statement->created_at}}</td>
                                <td>{{$statement->updated_at}}</td>

								<td>
									<a href="{{ url('storage', $statement->sample)}}" target="_blank">
									<button class="btn btn-primary" >View sample </button>	
									<a>
                                    <a href="#" data-target="#my_modal" data-toggle="modal" class="identifyingClass btn btn-primary" data-id="{{$statement->id}}">Update Sample</a>

                                </td>
                            
							</tr>
							@endforeach
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


    <div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Edit Statement</h4>
        </div>
        <div class="modal-body">
         
			<form action="{{route('update-sample')}}" method="POST">
	  @csrf
      <!-- <div class="form-group">
						<label for="bvn" class="form-label">Bank</label>
						
						
						<select type="text" name="bank" class="form-control"  id="bvn" placeholder="" required>
                        @foreach($banks as $bank)
                        <option value="">Choose Bank</option>
							<option  value="{{$bank->bank_name}}">{{$bank->bank_name}}</option>
							@endforeach
						</select>
					   
                    </div> -->
                 
      <div class="form-group">
						<label for="description"  class="form-label">Description <small>Short bank description</small></label>
                        <input type="text" name="description" class="form-control" id="account" placeholder="" >
                    </div>
                    
                    <div class="form-group">

            <select class="form-control" name = "status">
            <option value="">Choose Status</option>
            <option value="New">New</option>
			<option value="Pending">Pending</option>
            <option value="Converted">Converted</option>
          
            </select>
            <input type="hidden" name="id" id="hiddenValue" value="" />

                    </div>
        <div class="modal-footer">

		<button type="submit" class="btn btn-default">Update</button>
		</form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
@stop

@push('js')


<script type="text/javascript">
    $(function () {
        $(".identifyingClass").click(function () {
            var my_id_value = $(this).data('id');
            $(".modal-body #hiddenValue").val(my_id_value);
        })
    });

</script>


@endpush