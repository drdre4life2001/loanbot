@extends('layouts.app')

@section('title', "Bank Statement Sample")

@section('content')
    
	<h2 class="page-title clearfix">
		<span class="text">Sample Image For @{{ BankName }} </span>
    </h2>
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-5">
                    <img src="{{ asset('img/gallery/img2.jpg') }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
    
@endsection