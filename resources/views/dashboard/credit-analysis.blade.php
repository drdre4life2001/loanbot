@extends('layouts.app')

@section('title', 'Credit Analysis')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text">{{$customer[0]->first_name}}'s Credit Analysis</span>
		<div class="float-right">
			<a href="#" class="btn btn-link">
				<img src="assets/img/icons/printer.png" alt="" class="icon" height="20">
			</a>
			<!-- <a href="#" class="btn btn-primary btn-xl">Update Record</a>
			<a href="#" class="btn btn-danger btn-xl">Delete Record</a> -->
		</div>
	</h2>

	<div class="row mb-0 mb-md-4" style="font-size: 15px">
		<div class="col-12 col-md-3">
			<div class="card mb-4 mb-md-0 h-100">
				<div class="card-header">
					<div class="card-sub-title">BVN Details</div>
				</div>
				<div class="card-body">
					<p>{{$customer[0]->first_name}} {{$customer[0]->last_name}}</p>
					<p>{{$customer[0]->phone}}</p>
					<p>{{$customer[0]->date_of_birth}}</p>
					<p>{{$customer[0]->bvn}}<p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-3">
			<div class="card mb-4 mb-md-0 h-100">
				<div class="card-header">
					<div class="card-sub-title">Credit Decision</div>
				</div>
				<div class="card-body">
				
					   @if(isset($calculations))
					   <p>Maximum Loan Amount </p>
					<p>&#x20A6;<b>{{number_format($calculations['max_loan_amount'],2)}}</b></p>
						<p>Maximum Loan Repayment <b>&#x20A6;{{number_format($calculations['max_repayment'],2)}}</b></p>
						<p>Repayment Amount <b>&#x20A6;{{number_format($calculations['repayment_amount'],2)}}</b></p>
						<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Change loan tenure</button>
					   @else
					   <p>Salary information not found </p>

					   @endif
					<h2 class="text-center">
						<!--<img src="assets/img/icons/checkmark-green.png" alt="" class="d-inline" height="40">-->
						<span>
						    
						
						</span>
					</h2>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6">
			<div class="card mb-4 mb-md-0 h-100">
			@if(isset($average_salary))
			    <div class="card-header">
					<div class="card-sub-title"> Repayment Summary</div>
				</div>
			
				<div class="card-body">
					<div class="row text-center">
					<div class="col-6 border-right">
					@if(isset($repayment))
							<p>Total existing loan <b> &#x20A6;{{number_format($repayment)}}</b></p>
							
							<!-- <div class="text-center">
								<a href="#comment-modal" data-toggle="modal" class="btn btn-info btn-xl" style="min-width: 150px; max-width: 100%"></a>
							</div> -->
							<p><b>Comment</b></p>
							<p>{{$report->comment}}</p>
							<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#comment">Edit or add Comment</button>
							@endif
						</div>
						<div clas	s="col-6">
						
							<!-- <p>Repayments <b>N100,000</b></p> -->
						   
							<p>Average Salary <b>&#x20A6;{{number_format($average_salary)}}</b></p>
							<p>Last Salary Date <b>{{$last_salary_date}}</b></p>
						</div>
							@else
							<div class="card-header">
					<div class="card-sub-title"></div>
				</div>
			
						<div class="card-body">
					          <div class="row text-center">
					          <div class="col-6 border-right">
							
				           @endif
						</div>
						
			
					</div>
				</div>
			
			</div>
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">XDS RECORD ON {{$customer[0]->first_name}} <small>(₦)</small></div>
				</div>
				<div class="card-body">
					<table class="table table-borderless">
						<thead>

							<tr>
								<th>Subscriber Name</th>
								<th>Account Number</th>
								<th>Outstanding Balance</th>
								<th>Installment Amount</th>
								<th>Arrear Amount</th>
								<th>Facility Classification</th>
								<th>Account Status</th>
							</tr>
						</thead>
						<tbody>
						@if(!empty($xds_credit))
						@foreach($xds_credit as $credit)
							<tr> 
								<td>{{$credit->SubscriberName}}</td>
								<td>{{$credit->AccountNo}}</td>
									
								<td>{{$credit->CurrentBalanceAmt}}</td>
								<td>{{$credit->InstalmentAmount}}</td>
								<td>1</td>
								<td>{{$credit->PerformanceStatus}}</td>
								<td>{{$credit->AccountStatus}}</td>
							</tr>
						@endforeach
						
						@else
						<div class="container">
						<p class="textcenter">No XDS credit record found</p>
						@endif
					</div>
						
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div> -->

	<!-- <div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">CRC SUMMARY ON {{$customer[0]->first_name}} <small>(₦)</small></div>
				</div>
				<div class="card-body">
				<table class="table table-borderless">
						<thead>
							<tr>
								<th>Provider Source</th>
								<th>Number of Facilites</th>
								<th>Performing Facilities</th>
								<th>Non Performing</th>
								<th>Approved Amount</th>
								<th>Account Balance</th>
								<th>Overdue Amount</th>
								<th>Dishonored Cheques</th>
							</tr>
						</thead>
						<tbody>
						
						@if(!empty($crc_report))
							<tr> 
								<td>{{$crc_report->provider_source}}</td>
								<td>{{$crc_report->facilities_count}}</td>
								<td>{{$crc_report->performing_facility}}</td>	
								<td>{{$crc_report->non_performing}}</td>
								<td>{{$crc_report->approved_amount}}</td>
								<td>{{$crc_report->account_balance}}</td>
								<td>{{$crc_report->overdue_amount}}</td>
								<td>{{$crc_report->dishonored_cheques}}</td>
							</tr>

					@else
					<div class="container">
						<p class="textcenter">No CRC credit record found</p>
						@endif
					</div>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
 -->



	
		<div class="card card-table">
		<div class="card-header">
			<div class="card-title">
				<span>ACCOUNT STATEMENT</span>
				<span class="float-right">
					<a href="#" class="print btn btn-link">
						<img src="{{ asset('img/icons/printer.png') }}" alt="Print" height="25">
					</a>
					<a href="#collapse-4" class="collapse-toggle d-inline-block" data-toggle="collapse">
						<span class="mdi mdi-chevron-down"></span>
					</a>
				</span>
			</div>
		</div>
		<div class="card-body collapse show" id="collapse-4">
			<table class="table table-borderless table-condensed">
				<thead>
					<tr>
						<th>Value Date</th>
						<th>Debit</th>
						<th>Credit</th>
						<th>Balance</th>
						<th>Remarks</th>
						<th>Category</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
			
				@foreach($results as $result)
					<tr id="getter">
						<td>{{$result->transaction_date}}</td>
						<td>{{$result->debit}}</td>
						<td>{{$result->credit}}</td>
						<td>{{$result->balance}}</td>
						<td>{{$result->remarks}}</td>
						<td>{{$result->category}}</td>
						<td>
						<a href="#" data-target="#my_modal" data-toggle="modal" class="identifyingClass btn btn-info" data-id="{{$result->id}}">Update Category</a>


						<!-- <button type="button" data-role = "category" data-catid="{{$result->id}}" class="btn btn-info btn-lg" data-toggle="modal" data-target="#Category">Change Category</button> -->

						</td>
					
					</tr>
					@endforeach
					
				</tbody>
			</table>


			<div class="text-center pb-4">
				<a href="{{ route('full-statement',[$customer_id, $report_id])}}" class="btn btn-primary" style="border-radius: 3px; min-width: 200px">View Statement Summary</a>
			</div>
		</div>
	</div>
	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Set Tenure</h4>
      </div>
      <div class="modal-body">
	  <form action="{{route('update-tenure')}}" method="POST">
	  @csrf
  <div class="form-group">
    <label for="tenure">Loan Tenure:</label>
	<select class="form-control" name = "tenure">
            <option value="3">3</option>
            <option value="6">6</option>
            <option value="9">9</option>
            <option value="12">12</option>
            </select>
	<input type="hidden" name="customer_id" value="{{$customer_id}}" class="form-control">

  </div>

      </div>
      <div class="modal-footer">
	  <button type="submit" class="btn btn-default">Submit</button>
	  </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="comment" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Comment</h4>
      </div>
      <div class="modal-body">
	  <form action="{{route('update-comment')}}" method="POST">
	  @csrf
  <div class="form-group">
		<label for="tenure">Comment:</label>
		<input type="hidden" name="report_id" value="{{$report->id}}" class="form-control">

	<input type="textarea" name="comment" value="" class="form-control">

  </div>

      </div>
      <div class="modal-footer">
	  <button type="submit" class="btn btn-default">Update</button>
	  </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Change Category</h4>
        </div>
        <div class="modal-body">
         
			<form action="{{route('update-category')}}" method="POST">
	  @csrf
	  <div class="form-group">

            <select class="form-control" name = "category">
            <option value="">Choose category</option>
            <option value="Salary">Salary</option>
			<option value="Repayment">Repayment</option>
            <option value="Charges">Charges</option>
            <option value="POS">POS</option>
            <option value="Transfer">Transfer</option>
            </select>
            <input type="hidden" name="id" id="hiddenValue" value="" />
			<input type="hidden" name="customer_id" value="{{$customer_id}}" class="form-control">

        </div>
        <div class="modal-footer">

		<button type="submit" class="btn btn-default">Update</button>
		</form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
@stop
@push('js')


<script type="text/javascript">
    $(function () {
        $(".identifyingClass").click(function () {
            var my_id_value = $(this).data('id');
            $(".modal-body #hiddenValue").val(my_id_value);
        })
    });

</script>


@endpush
