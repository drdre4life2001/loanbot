@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text">Data Overview</span>
		<a href="#" class="btn btn-primary float-right btn-xl">Create Customer</a>
	</h2>

	<div class="row">
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">General Results</div>
					<div class="title"><b>9400</b> <small>Customers</small></div>
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<canvas height="100" class="bar-chart" data-type="bar" width="90">
									Your Browser Cannot Display This Chart
								</canvas>
							</div>
							<div class="col-5">
								<canvas height="100" class="doughnut-chart" data-type="doughnut"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">General Results</div>
					<div class="title"><b>9400</b> <small>Customers</small></div>
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<canvas height="100" class="bar-chart" data-type="bar" width="90">
									Your Browser Cannot Display This Chart
								</canvas>
							</div>
							<div class="col-5">
								<canvas height="100" class="doughnut-chart" data-type="doughnut"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="sub-title">General Results</div>
					<div class="title"><b>9400</b> <small>Customers</small></div>
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<canvas height="100" class="bar-chart" data-type="bar" width="90">
									Your Browser Cannot Display This Chart
								</canvas>
							</div>
							<div class="col-5">
								<canvas height="100" class="doughnut-chart" data-type="doughnut"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card card-table">
				<div class="card-header">
					<div class="card-title">All Customers</div>
				</div>
				<div class="card-body">
					<table class="table table-borderless">
						<thead>
							<tr>
								<th>Name</th>
								<th>BVN</th>
								<th>Date Of Birth</th>
								<th>Credit Score</th>
								<th>Phone Number</th>
								<th>Gender</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><b>Haruna Ahmadu</b></td>
								<td>20034564567</td>
								<td>14-OCT-2014</td>
								<td>600/1000</td>
								<td>09088776655</td>
								<td>Male</td>
								<td><button class="btn btn-primary">View Analysis</button></td>
							</tr>
							<tr>
								<td><b>Haruna Ahmadu</b></td>
								<td>20034564567</td>
								<td>14-OCT-2014</td>
								<td>600/1000</td>
								<td>09088776655</td>
								<td>Male</td>
								<td><button class="btn btn-primary">View Analysis</button></td>
							</tr>
							<tr>
								<td><b>Haruna Ahmadu</b></td>
								<td>20034564567</td>
								<td>14-OCT-2014</td>
								<td>600/1000</td>
								<td>09088776655</td>
								<td>Male</td>
								<td><button class="btn btn-primary">View Analysis</button></td>
							</tr>
							<tr>
								<td><b>Haruna Ahmadu</b></td>
								<td>20034564567</td>
								<td>14-OCT-2014</td>
								<td>600/1000</td>
								<td>09088776655</td>
								<td>Male</td>
								<td><button class="btn btn-primary">View Analysis</button></td>
							</tr>
							<tr>
								<td><b>Haruna Ahmadu</b></td>
								<td>20034564567</td>
								<td>14-OCT-2014</td>
								<td>600/1000</td>
								<td>09088776655</td>
								<td>Male</td>
								<td><button class="btn btn-primary">View Analysis</button></td>
							</tr>
							<tr>
								<td><b>Haruna Ahmadu</b></td>
								<td>20034564567</td>
								<td>14-OCT-2014</td>
								<td>600/1000</td>
								<td>09088776655</td>
								<td>Male</td>
								<td><button class="btn btn-primary">View Analysis</button></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop