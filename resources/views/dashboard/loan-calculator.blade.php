@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
	<h2 class="page-title clearfix">
		<span class="text">Credit Limit Generator</span>
	</h2>

	<div class="row">
		<div class="col-12">
			
			<div class="tab-content position-relative" style="z-index: 1">
				<div class="tab-pane fade active in show" id="generate">
					<div class="card">
					
						<div class="card-body p-5">
						@if ($errors->any())
  
  <ul>
	  <div style="color:red">
	  @foreach ($errors->all() as $error)
		  <li>{{ $error }}</li>
	  @endforeach
	  </div>
  </ul>
  @endif	
 
							<div class="row">
								<div class="col-md-6">
								<form method="POST" action="{{ route('loan-calculator') }} " enctype="multipart/form-data">
						          @csrf
								  <div class="form-group" id="salaries">
						<label for="" class="form-label">Salary 1</label>
						<input type="number" class="form-control salary-input mb-2" name="salary1">
						<label for="" class="form-label">Salary 2</label>
						<input type="number" class="form-control salary-input mb-2" name="salary2">
						<label for="" class="form-label">Salary 3</label>
						<input type="number" class="form-control salary-input mb-2" name="salary3">
					</div>
					<div class="form-text text-danger mb-3" id="enough" style="font-size: 14px"></div>
					<div class="text-left form-group">
						<div class="btn btn-info" id="add-salary">Add Salary</div>
					</div>

					<div class="form-group">
						<label for="" class="form-label">Existing loan</label>
						<input type="text" name="repayment" class="form-control">
					</div>

					<div class="form-group">
						<label for="" class="form-label">Loan Tenure</label>
						<select type="text" name="tenure" class="custom-select rounded">
							<!-- <option value="1">1</option>
							<option value="2">2</option> -->
							<option value="3">3</option>
							<!-- <option value="4">4</option> -->
							<option value="6">6</option>
							<option value="9">9</option>
							<option value="12">12</option>
						</select>
					</div>

                                   
									<div class="form-group">
										<button type="submit" class="btn btn-xl btn-primary btn-wide btn-radius">Calculate</button>
									</div>
								</form>
								</div>
								<div class="col-md-5 offset-md-1 d-flex align-items-center">
									<img src="{{ asset('img/icons/statement.png') }}" alt="" class="img-fluid">
								</div>
							</div>
						</div>
					</div>
				</div>


			
	
				
@stop

@push('js')
    
    
<script>
		$(function() {

			window.addSalary = function() {
				count = $('.salary-input').length;
				if (count > 5) {
					$('#enough').html('Only 6 salaries allowed');
					return false;
				}
				var label = $(document.createElement('label'))
					.addClass('form-label')
					.html(`Salary ${count + 1}`)
				var input = $(document.createElement('input'))
					.addClass('form-control salary-input mb-2')
					.attr('name', '[salary ${count + 1}`]');

				$('#salaries').append(label, input);
			}

			$('body').on('click', '#add-salary', function(event) {
				event.preventDefault();
				addSalary();
			});

		});
	</script>
@endpush
