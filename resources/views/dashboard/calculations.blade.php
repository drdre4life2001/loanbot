@extends('layouts.app')

@section('title', 'Credit Analysis')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text"> Loan Calculator</span>
		<div class="float-right">
			<a href="#" class="btn btn-link">
				<img src="assets/img/icons/printer.png" alt="" class="icon" height="20">
			</a>
			<!-- <a href="#" class="btn btn-primary btn-xl">Update Record</a>
			<a href="#" class="btn btn-danger btn-xl">Delete Record</a> -->
		</div>
	</h2>

	<div class="row mb-0 mb-md-10" style="font-size: 15px">
		
		<div class="col-12 col-md-8">
			<div class="card mb-4 mb-md-0 h-100">
				<div class="card-header">
					<div class="card-sub-title">Loan Decision</div>
				</div>
				<div class="card-body">
				
					   @if(isset($calculations))
					   <p>Maximum Loan Amount </p>
					<p>&#x20A6;<b>{{number_format($calculations['max_loan_amount'],2)}}</b></p>
						<p>Maximum Loan Repayment <b>&#x20A6;{{number_format($calculations['max_repayment'],2)}}</b></p>
						<p>Repayment Amount <b>&#x20A6;{{number_format($calculations['repayment_amount'],2)}}</b></p>
						<p>Average Salary</p>
					
					<p> <b>&#x20A6;{{number_format($average_salary)}}</b></p>
                        @endif
					  
                       <div class="text-center pb-4">
				<a href="{{ url('loan-calculator')}}" class="btn btn-primary" style="border-radius: 3px; min-width: 200px">Calculate again</a>
			</div>
					<h2 class="text-center">
						<!--<img src="assets/img/icons/checkmark-green.png" alt="" class="d-inline" height="40">-->
						<span>
						    
						
						</span>
					</h2>
				</div>
			</div>
		</div>
		
        
	
	
@stop
@push('js')


@endpush