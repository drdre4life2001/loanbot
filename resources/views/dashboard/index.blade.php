@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
	<h2 class="page-title clearfix">
		<span class="text">Dashboard</span>
		<div class="col-md-8" style="margin-left:0%;">
		@if(Session::has('msg'))
<div class="alert alert-danger alert-dismissible text-center col-md-6 close" data-dismiss="alert">
<p><h4>{{ Session::get('msg') }}  ×</h4></p>
</div>
@endif
	</h2>

	<div class="row" style="margin-bottom: 8rem">
		<div class="col-12 col-md-4">
			
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								
								<h1 class="font-weight-normal text-muted">{{$total_customers}}</h1>
								<p>Total Clients</p>
							</div>
							<div class="col-5 flex-center">
								<img src="{{ asset('img/icons/person-red.png') }}" alt="Total Clients" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
	
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<h1 class="font-weight-normal text-muted">{{$pdf_analysis}}</h1>
								<p>Total PDF Analyzed</p>
							</div>
							<div class="col-5 flex-center">
								<img src="{{ asset('img/icons/002-analytics.png') }}" alt="Total Analyzed" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="widget widget-tile">
				<div class="widget-head">
					<div class="widget-chart-container">
						<div class="row">
							<div class="col-7">
								<h1 class="font-weight-normal text-muted">{{$mbs_analysis}}</h1>
								<p>Total MBS Analyzed</p>
							</div>
							<div class="col-5 flex-center">
								<img src="{{ asset('img/icons/book.png') }}" alt="Open Cases" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">

			<div class="px-1">
				<ul class="nav nav-tabs">
					<li class="nav-item"><a href="#generate" data-toggle="tab" class="nav-link active">Generate Statement</a></li>
					<li class="nav-item"><a href="#analyze" data-toggle="tab" class="nav-link">Analyze Statement</a></li>
				</ul>
			</div>
			<div class="tab-content position-relative" style="z-index: 1">
				<div class="tab-pane fade active in show" id="generate">
					<div class="card">
						
						<div class="card-header">
							<div class="card-title">Generate MybankStatement  Token</div>
						</div>
						<div class="card-body p-5">
						@if ($errors->any())
  
  <ul>
	  <div style="color:red">
	  @foreach ($errors->all() as $error)
		  <li>{{ $error }}</li>
	  @endforeach
	  </div>
  </ul>
  @endif	
 
							<div class="row">
								<div class="col-md-6">
								<form method="POST" action="{{ route('get-ticket') }} " enctype="multipart/form-data">
						          @csrf
									<div class="form-group">
										<label for="bvn">BVN</label>
										<input type="text" name="bvn" class="form-control" id="bvn" placeholder="BVN" required>
									</div>
									<div class="form-group">
										<label for="bank">BANK</label>
										<select type="text" name="bank" class="form-control" id="bank">
										<option  value="" >Select Bank</option>
										@foreach($banks as $bank)
							             <option  value="{{$bank->bank_id}}">{{$bank->bank_name}}</option>
						             	@endforeach
										</select>
									</div>
									<div class="form-group mb-5 pb-2">
										<label for="account_no">ACCOUNT NUMBER</label>
										<input type="text" name="account" class="form-control" id="account_no" placeholder="ACCOUNT NUMBER">
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-xl btn-primary btn-wide btn-radius">Generate Statement</button>
									</div>
								</form>
								</div>
								<div class="col-md-5 offset-md-1 d-flex align-items-center">
									<img src="{{ asset('img/icons/statement.png') }}" alt="" class="img-fluid">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="analyze">
					<div class="card">
						<div class="card-header">
							<div class="card-title">Analyze Statement</div>
						</div>
						<div class="card-body p-5">
							<div class="row">
								<div class="col-md-6">
								<form method="POST" action="{{route('analyse-statement')}}" enctype="multipart/form-data"> 
					                 @csrf
									<div class="form-group">
										<label for="bvn">BVN</label>
										<input type="text" name="bvn" class="form-control" id="bvn" placeholder="BVN" required>
									</div>
									<div class="form-group">
										<label for="token">Ticket</label>
										<input type="text" name="ticket" class="form-control" id="token" placeholder="Ticket">
									</div>
									<div class="form-group">
										<label for="token">OTP</label>
										<input type="text" name="otp" class="form-control" id="token" placeholder="otp">
									</div> 
									
									
									<div class="form-group mb-5 pb-2">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="no_ticket">
											<label class="custom-control-label" for="no_ticket">I want to upload statement</label>
										</div>
									</div> 

									<div id="upload-statement">
									<div class="form-group" id="" style="">
										<label for="file">Upload Statement</label>
										<input type="file" name="statement" id="file" class="form-control">
									</div>

									<div class="form-group">
										<label for="bank">BANK</label>
										<select type="text" name="bank" class="form-control" id="upload-statement">
										<option  value="">Select Bank</option>
										@foreach($pdf_banks as $bank)
							             <option  value="{{$bank->id}}">{{$bank->bank_name}}</option>
						             	@endforeach
										</select>
									</div>
									</div>
									
									<div class="form-group twoToneCenter">

										<button type="submit" class="btn btn-xl btn-primary btn-wide btn-radius twoToneButton">Analyze</button>
									</div>
								</form>
								</div>
								<div class="col-md-5 offset-md-1 d-flex align-items-center">
									<img src="{{ asset('img/icons/pdf.png') }}" alt="" class="img-fluid">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@push('js')

	<script>
$(function(){
    
    var twoToneButton = document.querySelector('.twoToneButton');
    
    twoToneButton.addEventListener("click", function() {
        twoToneButton.innerHTML = "Please wait";
        twoToneButton.classList.add('spinning');
        
      setTimeout( 
            function  (){  
                twoToneButton.classList.remove('spinning');
                
            }, 10000);
    }, false);
    
});

		$(function() {
			$('body').on('change input', '#no_ticket', function() {
				upload = $('#upload-statement');
				if (this.checked) {
					upload.show();
				}else {
					upload.hide();
				}
			});
		});
	</script>

@endpush
