@extends('layouts.app')

@section('title', 'Credit Analysis')

@section('content')

	<h2 class="page-title clearfix">
		<span class="text">{{$customer[0]->first_name}}'s Statement Analysis</span>
		<div class="float-right">
			<a href="{{ route('credit-analysis',[$customer_id, $report_id])}}" class="btn btn-info ">Back to Full Statement
			</a>
			<!-- <a href="#" class="btn btn-primary btn-xl">Update Record</a>
			<a href="#" class="btn btn-danger btn-xl">Delete Record</a> -->
		</div>
	</h2>
	<div class="">
		<h3 class="clearfix">
			<span class="card-title">Statement Ananlysis</span>
		</h3>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="card">

				<div class="card-body">
					<canvas class="new-canvas" id="donut-chart-1">
						Chart Not Supported
					</canvas>
					<p>Debit Transactions</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-body">
					<canvas class="new-canvas" id="donut-chart-2">
						Chart Not Supported
					</canvas>
					<p>	Credit Transactions</p>
				</div>
			</div>
		</div>


		<br>
		<div class="col-md-12">
		<div class="card ">
		<br>
			<table class="table table-borderless table-condensed" style="text-align: left;">
				<tbody>
					<tr>
						<td>Opening Balance</td>
						@if(isset($summary))
						<td>&#x20A6;{{$summary['opening_balance']}}</td>
						<td><span class="mdi mdi-caret-up text-success" style="font-size: 20px"></span></td>
					</tr>
					<!-- <tr>
						<td>Closing Balance</td>
						<td>N2,339</td>
						<td><span class="mdi mdi-caret-down text-danger" style="font-size: 20px"></span></td>
					</tr> -->
					<tr>
						<td>Total Credit</td>
						<td>&#x20A6;{{$summary['total_credit']}}</td>
						<td><span class="mdi mdi-caret-up text-success" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Total Debit</td>
						<td>&#x20A6;{{$summary['total_debit']}}</td>
						<td><span class="mdi mdi-caret-down text-danger" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Maximum Balance</td>
						<td>&#x20A6;{{$summary['maximum_balance']}}</td>
						<td><span class="mdi mdi-caret-down text-dark" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Average Balance</td>
						<td>&#x20A6;{{$summary['average_balance']}}</td>
						<td><span class="mdi mdi-caret-up text-success" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Minimum Balance</td>
						<td>&#x20A6;{{$summary['minimum_balance']}}</td>
						<td><span class="mdi mdi-caret-up text-success" style="font-size: 20px"></span></td>
					</tr>
					@endif
					<tr>
						<td>Average Daily Balance</td>
						<td>&#x20A6;{{$avg_daily_balance}}</td>
						<td><span class="mdi mdi-caret-down text-dark" style="font-size: 20px"></span></td>
					</tr>
					<!-- <tr>
						<td>Overdraft</td>
						<td>No</td>
						<td><span class="mdi mdi-caret-down text-dark" style="font-size: 20px"></span></td>
					</tr>
					<tr>
						<td>Total Number Of Days In Overdraft</td>
						<td>0%</td>
						<td><span class="mdi mdi-caret-down text-dark" style="font-size: 20px"></span></td>
					</tr> -->
				</tbody>
			</table>
		</div>
	
	</div>
	<div class="col-md-12">
	<div class="">
		<h3 class="clearfix">
			<span class="card-title">Daily Balance</span>
		</h3>
	</div>
			<div class="card">
				<div class="card-body">
					<canvas class="new-canvas" id="line-chart-1">
						Chart Not Supported
					</canvas>
					<p>	Date</p>
				</div>
			</div>
		</div>


		</div>
	</div>
</div>

			<div class="text-center pb-4">
				<a href="{{ route('credit-analysis',[$customer_id, $report_id])}}" class="btn btn-primary" style="border-radius: 3px; min-width: 200px">Back to Full Statement</a>
			</div>
		</div>
	</div>




	
@stop
@push('js')

<script>
		$(function() {
			var randomScalingFactor = function() {
				return Math.round(Math.random() * 100);
			};
			var colors = {
				blue: '#007bff',
				green: '#00FF00',
				yellow: '#f8ca02',
				pink: '#f332cd',
				wine: '#b22222'
			}
			if ($('#donut-chart-1').length) {
				var config = {
					type: 'doughnut',
					data: {
						datasets: [
							{
								data: [{{$analysis['debit_transfer']}}, {{$analysis['charges']}}, {{$analysis['pos']}},{{$analysis['repayment']}}],
								backgroundColor: [colors.blue, colors.green, colors.wine, colors.yellow],
								label: 'Dataset 1'
							}
						],
						labels: [
							'ATM withdrawals',
							' Charges',
							' POS/WEB',
							' Repayments',
						]
					}
				}
				var ctx = document.querySelector('#donut-chart-1').getContext('2d');
				var chart = new Chart(ctx, config);
			}

			if ($('#donut-chart-2').length) {
				var config = {
					type: 'doughnut',
					data: {
						datasets: [
							{
								data: [{{$analysis['salary']}}, {{$analysis['credit_transfer']}}],
								backgroundColor: [colors.blue, colors.wine, colors.yellow],
								label: 'Dataset 1'
							}
						],
						labels: [
							'Total Salary',
							'Credit Transfer',

						]
					}
				}
				var ctx = document.querySelector('#donut-chart-2').getContext('2d');
				var chart = new Chart(ctx, config);
			}

			if ($('#donut-chart-3').length) {
				var config = {
					type: 'doughnut',
					data: {
						datasets: [
							{
								data: [randomScalingFactor(), randomScalingFactor()],
								backgroundColor: [colors.blue, colors.pink],
								label: 'Dataset 1'
							}
						],
						labels: [
							'Data Entry 1',
							'Data Entry 2',
						]
					}
				}
				var ctx = document.querySelector('#donut-chart-3').getContext('2d');
				var chart = new Chart(ctx, config);
			}

 
			if ($('#line-chart-1').length) {
				var ctx = document.querySelector('#line-chart-1').getContext('2d');

              
				grd = ctx.createLinearGradient(
					150.500, 0.000, 150.500, 311.000
					);

				// Add colors
				grd.addColorStop(0.433, 'rgba(0, 123, 255, 0.500)');
				grd.addColorStop(1.000, 'rgba(0, 123, 255, 0.100)');

				var config = {
					type: 'line',
					data: {
						datasets: [
							{
								data: [
									@foreach($daily_balance as $balance)
									'{{$balance->balance}}',
									@endforeach
								],
								borderColor: colors.blue,
								backgroundColor: grd,
								label: 'Daily Balance',
							}
						],
						labels: [
							@foreach($daily_balance as $date)
							'{{$date->transaction_date}}',
							@endforeach
						]
					}
				}
				var chart = new Chart(ctx, config);
			}
		});
	</script>

<script type="text/javascript">

Highcharts.chart('CreditAnalysis', {
		chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: 'Credit Tansactions'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
	credits: {
      enabled: false
  },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Credit Transactions',
        data: [
            ['Total Credit Transfer', {{$analysis['credit_transfer']}}],
            ['POS/WEB', {{$analysis['pos']}}],
            {
                name: 'Total Charges',
                y: {{$analysis['charges']}},
                sliced: true,
                selected: true
            }

        ]
    }]
});
</script>



@endpush
