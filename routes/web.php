<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard.index');
// });


Route::get('/login', function () {
    return view('login');
});

Route::get('/forgot-password', function () {
    return view('forgot-password');
});

Route::get('/reset-password', function () {
    return view('reset-password');
});

Route::get('/add-customer', function () {
    return view('add-customer');
});
Route::get('/ticket', function () {
    return view('customer.customer-ticket');
});

Route::get('/analysis', function () {
    return view('new.credit-analysis');
});

// Route::get('/deleted-customers', function () {
//     return view('deleted-customers');
// });

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['middleware' => [ 'auth']], function () {
    Route::match(['get', 'post'],'add-customer',['uses' => 'CustomerController@create'])->name('add-customer');
    Route::match(['get', 'post'],'automatic-statement',['uses' => 'CustomerController@mybankstatement'])->name('automatic-statement');
    Route::match(['get', 'post'],'analyse-statement',['uses' => 'CustomerController@analysestatement'])->name('analyse-statement');
    Route::match(['get','post'],'get-ticket',['uses' =>'CustomerController@automaticstatement'])->name('get-ticket');

    Route::get('credit-analysis/{id}/{statement}', 'HomeController@show')->name('credit-analysis'); 
    Route::get('full-statement/{id}/{report_id}', 'HomeController@full')->name('full-statement'); 
 
    Route::get('statement-sample', 'StatementController@index')->name('statement-sample');
    Route::post('update-sample', 'StatementController@update')->name('update-sample');
    Route::match(['get','post'],'create-sample',['uses' => 'StatementController@create'])->name('create-sample');

    Route::match(['get','post'],'fetch-credit-history',['uses' => 'CreditController@fetch'])->name('fetch-history');



  //  Route::post('search-result', 'HomeController@searchCustomer')->name('search-result');
  Route::post('update-category', 'HomeController@updatecategory')->name('update-category');
  Route::post('update-comment', 'HomeController@updatecomment')->name('update-comment');

  Route::post('update-tenure', 'HomeController@updatetenure')->name('update-tenure');
  Route::post('update-analysis', 'HomeController@update')->name('update-analysis');
  Route::match(['post', 'get'],'search', ['uses'=> 'HomeController@searchbvn'])->name('search');
  Route::match(['post', 'get'],'loan-calculator', ['uses'=> 'HomeController@loancalculator'])->name('loan-calculator');
  Route::post('calculations',  'HomeController@calculations')->name('calculations');


});
Auth::routes();
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
//Route::get('/success', 'HomeController@index')->name('dashboard');

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/all-customers', 'CustomerController@index')->name('all-customers');

